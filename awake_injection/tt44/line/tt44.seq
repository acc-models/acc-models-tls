!-------------------------------------------------------
!   elements
!-------------------------------------------------------

//-------------- bending magnets
mbh: rbend, 
l := 0.1755 , 
apertype = ellipse , 
aperture = {0.02,0.02}, 
fint := fint_h_var, 
hgap = 0.035, 
k1 := mbh_k1 ; 		! bending magnet, horizontal

mbv: rbend, 
l := 0.1755 , 
apertype = ellipse , 
aperture = {0.02,0.02}, 
fint := fint_v_var, 
hgap = 0.035 ;   		! bending magnet, vertical  

fint_h_var = 0.25;
fint_v_var = 0.09;

!specification magnetic lenght 0.1755
!specification mechanical lenght 0.294


//-------------- quadrupoles
quad: quadrupole, l := 0.0708, apertype = ellipse, aperture = {0.02,0.02}; 
!specification magnetic length 0.0718, but the measured gradient correspond to 
!a magnetic lenght slightly smaller, i.e. 0.0708 m => this is very important to take
!into account



//-------------- correctors
cor: kicker, l := 0.0,   apertype = ellipse, aperture = {0.02,0.02}; 
!cor: kicker, l := 0.0406,   apertype = ellipse, aperture = {0.02,0.02}; 
!cor	    : kicker	  , l := 0.1651; !magnetic length
!specification magnetic length 0.1654
!specification mechanical lenght 0.073


//-------------- beam position monitors
!bpm         : monitor     , l := 0.207,   apertype = ellipse, aperture = {0.02,0.02};
bpm         : monitor     , l := 0.0,   apertype = ellipse, aperture = {0.02,0.02};
bpmshort    : monitor     , l := 0.207,   apertype = ellipse, aperture = {0.02,0.02};
bpmlong     : monitor     , l := 0.207,   apertype = ellipse, aperture = {0.02,0.02};


//-------------- beam observation tv monitors based on screens

btv         : instrument     , l := 0.273,   apertype = ellipse, aperture = {0.02,0.02};
btv2	    : instrument     , l := 0.354,   apertype = ellipse, aperture = {0.02,0.02};


//-------------- markers
start_line  : marker      , l := 0;
end_line    : marker      , l := 0;	! apertype = rectangle, aperture = {0.00025,0.00025};
mon         : instrument     , l := 0;

//-------------- beamstopper
beam_stopper : instrument , l := 0.15;			!0.069775;



//-------------- elements proton and secondary beam line
bpg412445   : instrument  , l := 0.248 , aperture = ellipse, aperture = {0.060,0.060}; !old 0.248
otr2	    : instrument  , l := 0.35 , aperture = ellipse, aperture = {0.06,0.06};

plasmacell:   instrument , l = 10    , aperture=ellipse ,aperture={0.020 ,0.020}  ;
iris: 		  instrument , l = 0.001 , aperture=ellipse ,aperture={0.005 ,0.005}  ;

table:        instrument , l = 2 ;

mqnbr       : quadrupole  , l := 0.285;


mbxfb       : sbend       , l := 1;


btvsps      : instrument     , l = 0.448;

heater	    : solenoid	  , l = 0.025 , ks = 0.00017/0.033;

!-------------------------------------------------------
!  start of sequence
!-------------------------------------------------------

! Rememeber to remove the correctors when adding the BPMs thickness

! central positions
tt43: sequence, refer=centre, l = 25 ;
begi.1000: start_line, at= 0 ;

bpm_offset = 0.0067;
bpm_offset2 = 0.0087;

line_shift = 0.640225486;

correction_shift = 1.84615;
/////////////// matching section ///////////////

bpm.430016: bpm, at= 0.2;
mcawa.430017: cor, at= 0.01, from = bpm.430016;

mqawd.430031: quad, at=  1;
mqawf.430034: quad, at=  2.6;
mqawd.430037: quad, at=  4.2;

bpm.430039: bpm, at= 4.5;
mcawa.430040: cor, at= 0.01, from = bpm.430039;

btv.430042: btv, at=4.8;

/////////////// achromat ///////////////


mbawh.430300: mbh, at= 5.2136;
bpm.430103: bpm, at=  -3.0, from= mqawd.430128;
mcawa.430105: cor, at= 0.01, from = bpm.430103;
btv.430106: btv, at=-2.3, from=mqawd.430128;


mqawf.430118: quad, at= -2, from= mqawd.430128;
mqawd.430128: quad, at= 8.713049999999999;
mqawf.430205: quad, at= 2, from=mqawd.430128;

bpm.430209: bpm, at=  3.0, from=mqawd.430128;
mcawa.430210: cor, at= 0.01, from = bpm.430209;

///////////////	common beam line ///////////////

mbawh.412343: mbh, at:= 12.2125 ;

mqawd.412344: quad, at:= -3, from= mqawf.412346;
bpm.412345: bpm, at= -2.9, from=bpm.412347;
mcawa.412345: cor, at= 0.01, from=bpm.412345;
mqawf.412346  : quad, at:= 18.1;
bpm.412347: bpm, at= 18.3;


mcawa.412347: cor, at= 0.01, from=bpm.412347;

mqawd.412347: quad, at:= +3, from= mqawf.412346;

bpm.412349: bpm, at= 21.3;
mcawa.412349: cor, at= 0.01, from=bpm.412349; !21.4 m
btv.412350: otr2, at= 21.6; 				

btv.412353: otr2, at = 22.1;		
bpm.412353: bpm, at= 22.6; 		
mcawa.412353: cor, at= 22.61;  
                                


///////////////	plasma cell ///////////////

plasma.e: Mon, AT:= 23.0077 ; 
btv.412354: mon, at = -0.711251, from=btv.exp_vap;	
btv.exp_vap: mon, at = -0.1, from=iris1;				! btv for electrons and protons
iris1: mon, at= 0.9, from=plasma.e;
plasma_merge: mon, at= 0.9, from=plasma.e;       			! merging_point = 0.5m (standard)
!iris2: mon, at= 10.645, from=iris1;
!plasma.s: mon, at=0.6, from=iris2;



endsequence;





!=======================================

kmbawh430300 :=  0.34925 ;		!angle horizontal bending 1 mbh2003
kmbawh412343 :=  -0.34925 ;  !angle horizontal bending 2 mbh2005
mbh_tilt 	= -0.105103009315855 ;



//————strength dipoles

mbawh.430300 , angle :=    kmbawh430300;
!mbawh.412343 , angle :=    kmbawh412343, tilt := mbh_tilt, e1 := var_e1, e2 := var_e2;
mbawh.412343 , angle :=    kmbawh412343;

//————strength quadrupoles
mqawd.430031 , k1    :=  kqd430031 ;
mqawf.430034 , k1    :=  kqf430034 ;
mqawd.430037 , k1    :=  kqd430037 ;

!mqawd.430109 , k1    :=  kqd430109 ;

mqawf.430118 , k1    :=  kqf430118;
mqawd.430128 , k1    :=  kqd430128;
mqawf.430205 , k1    :=  kqf430118;

!mqawf.430311 , k1    :=  kqf430311  ;

mqawd.412344 , k1    :=  kqd412344 ;
mqawf.412346 , k1    :=  kqf412346 ;
mqawd.412347 , k1    :=  kqd412347  ;


//————strength kickers
!mcawa.430029, vkick  := 0.00, 	hkick = 0;
!mcawa.430040, vkick  := 0.00, 	hkick = 0;
!mcawa.430104, vkick  := 0.00, 	hkick = 0;
!mcawa.430130, vkick  = 0, 	hkick = 0;
!mcawa.430204, vkick  = 0, 	hkick = 0;
!mcawa.430309, vkick := kcv430309, hkick = 0;

!mcawa.412344, vkick := kcv412344, hkick := kch412344;

!mcawa.412345, vkick := kcv412345, hkick := kch412345;
!mcawa.412347, vkick := kcv412347, hkick := kch412347;
!mcawa.412349, vkick := kcv412349, hkick := kch412349;
!mcawa.412353, vkick := kcv412353, hkick := kch412353;



!=======================================
! Theoretical initial conditions
!=======================================

BETX0=5;
ALFX0=0;
DX0=0;
DPX0=0;
BETY0=5;
ALFY0=0;
DY0=0.0;
DPY0=0.0;
