!==============================================================================================
! MADX file for DE-LNI optics
!
! M.A. Fraser, D. Gamba, F.M. Velotti
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "ELENA/DE-LNI optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/
system, "ln -fns ../../../../acc-models-elena elena_repo";
system, "ln -fns ./../../lne lne_repo";
system, "ln -fns ./../../delnslni delnslni_repo";


/*******************************************************************************
 * beam
 *******************************************************************************/
 beam, particle=antiproton;

 mass=beam->mass;

 Ekin=0.0053; ! Assuming AD extraction energy of 5.3 MeV

 gamman=(Ekin/mass)+1;
 beta=sqrt(-((1/gamman)^2)+1);
 value,beta;
 pcn=sqrt((mass^2)*((gamman^2)-1));
 
 beam, particle=antiproton,pc=pcn,exn=6E-6/6,eyn=4E-6/6;

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};

/*****************************************************************************
 Calculate initial condition for matching in the ring
*****************************************************************************/

call, file = "delnslni_repo/load_elena_injection_ad.madx";

 set, format="22.10e";
exec, calculate_match(twiss_elena_matched.tfs);

/*****************************************************************************
 * DE
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "delnslni_repo/de/de.ele";
 call, file = "delnslni_repo/de/de_lni_k.str";
 call, file = "delnslni_repo/de/de.seq";
 !call, file = "delnslni_repo/de/de.dbx"; !Presently no aperture database: to be updated
 option, echo;
 
 /*****************************************************************************
 * LNI
 *
 *****************************************************************************/
 option, -echo;
 call, file = "delnslni_repo/lni/lni.ele";
 call, file = "delnslni_repo/lni/lni.seq";
 !call, file = "delnslni_repo/lni/lni.dbx"; !Presently no aperture database: to be updated
 option, echo; 
 
 SEQEDIT, SEQUENCE=lni; FLATTEN; ENDEDIT;
 
/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/
 delni: sequence, refer=ENTRY, l = 8.28324 + 12.34666;
   de                    , at =      0;
   lni                   , at = 8.28324;
 endsequence;

 SEQEDIT, SEQUENCE=delni; FLATTEN; ENDEDIT;

 /*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
 call, file = "./ad_extraction.inp";
 exec, set_ini_conditions();
 
/*******************************************************************************
 * Run twiss for DE-LNI and stitch result
 *******************************************************************************/

use, sequence= delni;  
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta, label=tl_final_cond, place = lni.end;
twiss, beta0=initbeta0, file = "twiss_de_lni.tfs", table = twiss_de_lni;

len_twiss = table(twiss_de_lni, tablelength);
value, len_twiss;
    
i = 2;
option, -info;
create,table=trajectory, column=_NAME,S,L, _KEYWORD, BETX,ALFX, x, px, dx, dpx, MUX,BETY,ALFY,Y,DY,PY,DPY,MUY, k1l;
while(i < len_twiss){

SETVARS, TABLE=twiss_de_lni, ROW=i;
x0 = x;
px0 = px;

fill, table=trajectory;

i = i + 1;

};


 set, format="22.10e";
exec, calculate_injection(twiss_elena_stitched.tfs);

!exec, set_ini_conditions();

! Make one single tfs file for both ring and transfer line 

len_twiss_ring = table(twiss_ring, tablelength);
value, len_twiss_ring;

i = 2;
option, -info;
while(i < len_twiss_ring){

    if(i == 2){
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss_ring, ROW=i;
    s = s + s0;
    fill, table=trajectory;

    i = i + 1;
};
option, info;
write, table=trajectory, file="twiss_de_lni_elena_nom_complete.tfs";


/*************************************
* Cleaning up
*************************************/

system, "rm lne_repo";
system, "rm elena_repo";
system, "rm delnslni_repo";

stop;
