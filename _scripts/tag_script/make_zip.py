from pathlib import Path
from fnmatch import fnmatch
import os
import shutil
import subprocess
import sys
import zipfile


def read_requirements(file_name):
    req = {}
    with open(file_name, 'r') as f:
        for line in f:
            dir, machine = line.strip().split(':')
            req[dir] = [ele.strip() for ele in machine.split(',')]
    return req


def clone_repos(req_file, branch, root_dir):
    for folder in req_file.keys():
        machine_needed = req_file[folder]
        for machine in machine_needed:
            repo_ip = f'https://gitlab.cern.ch/acc-models/acc-models-{machine}'
            destination = root_dir / folder / f'{machine}_repo'
            command = f'git clone --depth 1 -b {branch} {repo_ip} {destination}'
            process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
            process.wait()


def delete_repos(req_file, root_dir):
    for folder in req_file.keys():
        machine_needed = req_file[folder]
        for machine in machine_needed:
            destination = root_dir / folder / f'{machine}_repo'
            shutil.rmtree(str(destination))
            
            
def make_zip(input_dir, output_file, exclude_dirs=(), exclude_files=()):
    
    if input_dir.is_absolute():
        os.chdir(input_dir)
        input_dir = '.'
    
    file_structure = os.walk(input_dir)
    zf = zipfile.ZipFile(output_file, "w")

    for dirname, subdirs, files in file_structure:

        for exclude_dir in exclude_dirs:
            if exclude_dir in subdirs:
                subdirs.remove(exclude_dir)
        
        zf.write(dirname)
        for filename in files:
            is_excluded = any(fnmatch(filename, pattern) for pattern in exclude_files)
            if '.zip' not in filename and not is_excluded:
                zf.write(os.path.join(dirname, filename))
    zf.close()
    

def main():
    root_dir = Path(__file__).resolve().parents[2]

    req = read_requirements(root_dir / 'repo_requirements_test.txt')

    print(req)
    clone_repos(req_file=req,
                branch='2021',
                root_dir=root_dir)
            
    make_zip(root_dir, 
             root_dir / 'jmad-modelpack.zip',
             exclude_dirs=('.git','supplementary','_scripts'),
             exclude_files=('*.tfs','*.pkl','*.twiss'))
    
    delete_repos(req_file=req,
                 root_dir=root_dir)    


if __name__ == "__main__":
    main()
