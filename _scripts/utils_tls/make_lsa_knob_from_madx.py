import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import typing as t

h_corr_ti2 = [
    '"RBIH.610104/K"',
    '"RBIH.610206/K"',
    '"RCIBH.20804/K"',
    '"RCIBH.21004/K"',
    '"RCIBH.21604/K"',
]
v_corr_ti2 = [
    '"RCIBV.20304/K"',
    '"RCIBV.20504/K"',
    '"RCIBV.21104/K"',
    '"RCIBV.21304/K"',
]

h_corr_ti8 = ['"RBIH.400107/K"', '"RBIH.400309/K"']
scaling_ti8 = [3.0, 4.0]
v_corr_ti8 = [
    '"RCIBV.400097/K"',
    '"RBIV.400293/K"',
    '"RCIBV.80104/K"',
    '"RCIBV.80704/K"',
]

converter_names = {
    "ti8": {"hor": h_corr_ti8, "ver": v_corr_ti8},
    "ti2": {"hor": h_corr_ti2, "ver": v_corr_ti2},
}

scaling = {ele: scale for ele, scale in zip(h_corr_ti8, scaling_ti8)}


def create_files(
    path_tables: Path,
    tl_name: str,
    plane: str,
    converter_names: t.List[str],
    output_dir: str = "",
):
    phases, *kicks_all = np.loadtxt(
        path_tables / f"{plane}_knobs_liu_q26.madx", skiprows=8, unpack=True
    )

    kicks = [pair for pair in zip(*kicks_all)]

    plt.figure()
    for kick, converter in zip(kicks_all, converter_names):
        plt.plot(phases, kick * 1e6, label=converter)
    plt.xlabel(r"$\mu$ (deg)")
    plt.ylabel(r"kick ($\mu$rad)")
    plt.minorticks_on()
    plt.legend(loc="best")

    for ks_phase, phase in zip(kicks, phases):
        print(phase)
        output_file_name = (
            path_tables
            / output_dir
            / f"liu_{tl_name}_1sigma_{plane}_ph{int(phase)}.tfs"
        )
        print(output_file_name)
        name_knob = f"liu_{tl_name}_1sigma_{plane}_ph{int(phase)}"
        with open(output_file_name, "w") as f:
            f.write(
                '@TITLE  "' + name_knob + '" \n'
                '@ NAME %05s  "TWISS" \n'
                '@ TIME %08s  "12.40.00 \n'
                "* NAME KICK \n"
                "$ %s       %le\n"
            )
            for converter, kick in zip(converter_names, ks_phase):
                print(converter, kick)
                line_to_write = f"{converter}   {kick / scaling.get(converter, 1.0)}\n"
                print(line_to_write)
                f.write(line_to_write)


# path_ti2 = '/afs/cern.ch/work/f/fvelotti/private/TI2/operation/'

path_ti8 = Path(
    "/home/fvelotti/acc-models-tls/sps_extraction/tt60ti2_q26/line/knob_tables/"
)

# create_files(5, path_ti2, 'ti2', 'hor', h_corr_ti2, figure=1)
# create_files(4, path_ti2, 'ti2', 'ver', v_corr_ti2, figure=2)

create_files(path_ti8, "ti2", "hor", h_corr_ti2, output_dir="./")
create_files(path_ti8, "ti2", "ver", v_corr_ti2, output_dir="./")

plt.show()
