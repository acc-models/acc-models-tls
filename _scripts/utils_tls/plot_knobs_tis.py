import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path


def readtfs(filename, usecols=None, index_col=0):
    header = {}
    nskip = 1
    closeit = False

    try:
        datafile = open(filename, "r")
        closeit = True
    except TypeError:
        datafile = filename

    for line in datafile:
        nskip += 1
        if line.startswith("@"):
            entry = line.strip().split()
            header[entry[1]] = eval(" ".join(entry[3:]))
        elif line.startswith("*"):
            colnames = line.strip().split()[1:]
            break

    if closeit:
        datafile.close()

    table = pd.read_csv(
        filename,
        delim_whitespace=True,
        skipinitialspace=True,
        skiprows=nskip,
        names=colnames,
        usecols=usecols,
        index_col=index_col,
    )

    return header, table


def rotate_ti8(x_f, px_f, y_f, py_f):
    """
    Rotation of the 4D space - delta is the rotation angle of the line to match LHC psi
    @param x_f: x_0
    @param px_f: px_0
    @param y_f: y_0
    @param py_f: py_0
    @return: x, px, y, py
    """
    delta = -1 * (0.013432861 - 0.066205508)

    rot_matrix = np.array(
        [
            [np.cos(delta), 0.0, np.sin(delta), 0.0],
            [0.0, np.cos(delta), 0.0, np.sin(delta)],
            [-1.0 * np.sin(delta), 0.0, np.cos(delta), 0.0],
            [0.0, -1.0 * np.sin(delta), 0.0, np.cos(delta)],
        ]
    )

    phase_space = np.array([x_f, px_f, y_f, py_f])
    new = np.zeros((4, len(x_f)), float)

    for i in range(0, len(x_f)):
        new[:, i] = np.inner(rot_matrix, phase_space[:, i])

    x_f = new[0]
    px_f = new[1]
    y_f = new[2]
    py_f = new[3]

    return x_f, px_f, y_f, py_f


def rotate_ti2(x_f, px_f, y_f, py_f):
    """
    Rotation of the 4D space - delta is the rotation angle of the line to match LHC psi
    @param x_f: x_0
    @param px_f: px_0
    @param y_f: y_0
    @param py_f: py_0
    @return: x, px, y, py
    """
    delta = -1 * (0.004170381943 + 0.01578621)

    rot_matrix = np.array(
        [
            [np.cos(delta), 0.0, np.sin(delta), 0.0],
            [0.0, np.cos(delta), 0.0, np.sin(delta)],
            [-1.0 * np.sin(delta), 0.0, np.cos(delta), 0.0],
            [0.0, -1.0 * np.sin(delta), 0.0, np.cos(delta)],
        ]
    )

    phase_space = np.array([x_f, px_f, y_f, py_f])
    new = np.zeros((4, len(x_f)), float)

    for i in range(0, len(x_f)):
        new[:, i] = np.inner(rot_matrix, phase_space[:, i])

    x_f = new[0]
    px_f = new[1]
    y_f = new[2]
    py_f = new[3]

    return x_f, px_f, y_f, py_f


path_ti8 = Path(
    "/home/fvelotti/acc-models-tls/sps_extraction/tt60ti2_q26/line/matching_output/"
)

gamma = np.sqrt(0.938**2 + 450**2) / 0.938

emit = 3.5e-6 / gamma


_, twiss_line = readtfs(path_ti8 / "twiss_knobs_h_0_liu.tfs", index_col="NAME")

twiss_line.columns = [col.lower() for col in twiss_line.columns]

msi = "MSIB.ENTR"

plane = dict(h="x", v="y")

figures = {"phase_space": {"h": 1, "v": 2}, "along_line": {"h": 3, "v": 4}}

for j, vh in enumerate(["h", "v"]):
    for i in range(0, 7):
        _, twiss_phase = readtfs(path_ti8 / f"twiss_knobs_{vh}_{i * 30}_liu.tfs")
        twiss_phase.columns = [col.lower() for col in twiss_phase.columns]

        plt.figure(figures["along_line"][vh])
        plt.plot(
            twiss_phase.s * 1e-3,
            twiss_phase[plane[vh]] / np.sqrt(emit * twiss_phase[f"bet{plane[vh]}"]),
            label="phase: " + str(i * 30),
        )

        # x, px, y, py = rotate_ti8(x, px, y, py)
        plt.figure(figures["phase_space"][vh])
        plt.plot(
            twiss_phase[plane[vh]].loc[msi]
            * 1e3
            / np.sqrt(twiss_phase[f"bet{plane[vh]}"].loc[msi]),
            -1e3
            * (
                twiss_phase[f"bet{plane[vh]}"].loc[msi]
                * twiss_phase[f"p{plane[vh]}"].loc[msi]
                + twiss_phase[f"alf{plane[vh]}"].loc[msi]
                * twiss_phase[plane[vh]].loc[msi]
            )
            / np.sqrt(twiss_phase[f"bet{plane[vh]}"].loc[msi]),
            "x",
            markersize=10,
            label="phase: " + str(i * 30),
        )

plt.figure(figures["along_line"]["h"])
plt.title("TI8")
plt.xlabel("s (km)")
plt.ylabel(r"x / $\sqrt{\beta_x\epsilon_x}$")
plt.legend(loc="best")

plt.figure(figures["along_line"]["v"])
plt.title("TI8")
plt.xlabel("s (km)")
plt.ylabel(r"y / $\sqrt{\beta_y\epsilon_y}$")
plt.legend(loc="best")

plt.figure(figures["phase_space"]["h"])
plt.xlabel(r"$\bar{x}$ / $\sqrt{m}$")
plt.ylabel(r"$\bar{x}$' / $\sqrt{m}$")
phi = np.linspace(0, 2 * np.pi, 200)
plt.plot(1e3 * np.sqrt(emit) * np.cos(phi), 1e3 * np.sqrt(emit) * np.sin(phi))
plt.legend(loc="best")

plt.figure(figures["phase_space"]["v"])
plt.xlabel(r"$\bar{x}$ / $\sqrt{m}$")
plt.xlabel(r"$\bar{y}$ / $\sqrt{m}$")
plt.ylabel(r"$\bar{y}$' / $\sqrt{m}$")
plt.plot(1e3 * np.sqrt(emit) * np.cos(phi), 1e3 * np.sqrt(emit) * np.sin(phi))
plt.legend(loc="best")

plt.show()
