#!/bin/bash
find ./.. -type f -name 'general*.pdf' -delete
find ./.. -type f -name 'general*.html' -delete
find ./.. -type f -name 'index*' -delete
find ./.. -type f -name 'nav*yml' -delete
find ./.. -type f -name 'branch.txt' -delete

