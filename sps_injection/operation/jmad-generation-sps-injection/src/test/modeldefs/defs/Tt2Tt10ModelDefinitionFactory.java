/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package modeldefs.defs;

import java.util.HashSet;
import java.util.Set;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

public class Tt2Tt10ModelDefinitionFactory implements ModelDefinitionFactory{
    
    private Set<OpticsDefinition> createOpticsDefinitions() {
        Set<OpticsDefinition> definitionSet = new HashSet<>();
        definitionSet.add(new OpticsDefinitionImpl("TT2TT10SPS-LHC-Q20-2021v1", new CallableModelFileImpl(
                "operation/beta0.inp", ModelFileLocation.REPOSITORY),new CallableModelFileImpl(
                "tt2tt10_lhc_q20/stitched/jmad/tt10sps_lhc_q20_savedseq.seq", ModelFileLocation.REPOSITORY),
                new CallableModelFileImpl(
                        "tt2tt10_lhc_q20/stitched/jmad/tt10sps_lhc_q20.inp", ModelFileLocation.REPOSITORY)
                ));
        definitionSet.add(new OpticsDefinitionImpl("TT2TT10SPS-LHC-Q26-2021v1", new CallableModelFileImpl(
                "operation/beta0.inp", ModelFileLocation.REPOSITORY),new CallableModelFileImpl(
                "tt2tt10_lhc_q26/stitched/jmad/tt10sps_lhc_q26_savedseq.seq", ModelFileLocation.REPOSITORY),
                new CallableModelFileImpl(
                        "tt2tt10_lhc_q26/stitched/jmad/tt10sps_lhc_q26.inp", ModelFileLocation.REPOSITORY)

                ));
        definitionSet.add(new OpticsDefinitionImpl("TT2TT10SPS-SFTPRO-2021v1", new CallableModelFileImpl(
                "operation/beta0.inp", ModelFileLocation.REPOSITORY),new CallableModelFileImpl(
                "tt2tt10_sftpro/stitched/jmad/tt10sps_sftpro_savedseq.seq", ModelFileLocation.REPOSITORY),
                new CallableModelFileImpl(
                        "tt2tt10_sftpro/stitched/jmad/tt10sps_sftpro.inp", ModelFileLocation.REPOSITORY)
 
                ));
        definitionSet.add(new OpticsDefinitionImpl("TT2TT10SPS-PB82-Q26-2021v1", new CallableModelFileImpl(
                "operation/beta0.inp", ModelFileLocation.REPOSITORY),new CallableModelFileImpl(
                "tt2tt10_pb82_q26/stitched/jmad/tt10sps_pb_q26_savedseq.seq", ModelFileLocation.REPOSITORY),
                new CallableModelFileImpl(
                        "tt2tt10_pb82_q26/stitched/jmad/tt10sps_pb_q26.inp", ModelFileLocation.REPOSITORY)
 
                ));
        

        
        return definitionSet;
    }
    
    @Override
    public JMadModelDefinition create() {
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("TT10SPS");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setRepositoryPrefix("..");
        offsets.setResourcePrefix(".");
        modelDefinition.setModelPathOffsets(offsets);

        for (OpticsDefinition opticsDefinition : createOpticsDefinitions()) {
            modelDefinition.addOpticsDefinition(opticsDefinition);
        }
       
        
        
        modelDefinition.setDefaultOpticsDefinition(modelDefinition.getOpticsDefinitions().get(0));

        /*
         * SEQUENCE
         */

       
        SequenceDefinitionImpl tt10sps = new SequenceDefinitionImpl("tt10sps",null);
        modelDefinition.setDefaultSequenceDefinition(tt10sps);
        RangeDefinitionImpl tt10spsrange = new RangeDefinitionImpl(tt10sps, "ALL", createTT10InitialConditions());
        tt10sps.setDefaultRangeDefinition(tt10spsrange);

        return modelDefinition;
    }

    /**
     * Twiss initial conditions for transferline TT2TT10
     */
    private final TwissInitialConditionsImpl createTT10InitialConditions() {
        
        
        TwissInitialConditionsImpl twissInitialConditions = new TwissInitialConditionsImpl("tt10sps-twiss");
        
        twissInitialConditions.setSaveBetaName("TT2.INITBETA0");
       
        twissInitialConditions.setCalcAtCenter(true);
        twissInitialConditions.setClosedOrbit(false);
      
        return twissInitialConditions;

    }
   
    
    

}
