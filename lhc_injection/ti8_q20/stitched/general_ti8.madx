!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Sequence for LHC injection stitched model for TI8
! F.M. Velotti, M. Fraser
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

option, RBARC=FALSE;
option, echo;

title,   "TI8 line for LHC";
/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../../sps_extraction/tt40ti8 ti8_repo";
system, "ln -fns ./../../../sps_extraction/tt40ti8_q20/line ti8_line_repo";
system, "ln -fns ./../../../sps_extraction/sps_ext_elements extr_macros";

set, format="22.10e";
option, echo, warn, info;

/***************************************
* Load model
***************************************/

call,    file = "ti8_repo/ti8.seq";
call,    file = "ti8_repo/ti8_apertures.dbx";
call,    file = "ti8_line_repo/ti8_liu.str";

! Load LHC sequnce
system,"[ ! -e lhc_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-lhc -b 2022 lhc_repo";

option,  echo, warn, info;

beam,    sequence=ti8, particle=proton, pc= 450;
use,     sequence=ti8;

! Errors on b2 and b3 as measured
call, file="ti8_repo/mbi_b3_error.madx";

! Using original initial conditions as used for matching
call, file = "ti8_line_repo/../stitched/sps_tt40_ti8_lhc_q20.inp";

/* call, file = "ti8_line_repo/../stitched/sps_tt40_ti8_lhc_q20_from_stitched_kickers.inp"; */
set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=0,
      PX=0,
      Y=0,
      PY=0;

};

exec, set_ini_conditions();

show, INITBETA0;

select, flag=twiss, column=NAME, KEYWORD, S, L, TILT, KICK, HKICK, VKICK, ANGLE, K0L, K0SL, K1L, K1SL, K2L, K2SL, K3L, K3SL, BETX, BETY, X, PX, Y, PY, DX, DPX, DY, DPY, ALFX, ALFY, MUX, MUY;
set, format="22.10e";
Twiss,file="./twiss_ti8.tfs",beta0=INITBETA0, x=x0,px=xp0;


/***************************************
* Adding LHC
***************************************/

option, -warn;
call, file = "./lhc_inj_b2.seq";
option, warn;

Linjdsl7 = 4011.308990594; Lti8 = 2697.257490800;
ti8lhcb2 : sequence, refer=entry, l= Linjdsl7 + Lti8 ;
 TI8, at=0;
 INJDSL7, at=0, from=lhcinj.ti8;
endsequence;

beam,    sequence=ti8lhcb2, particle=proton, pc= 450;
use,     sequence=ti8lhcb2;

on_x1:=0; on_sep1:=0;
on_x2:=0; on_sep2:=0; on_alice:=0;
on_x5:=0; on_sep5:=0;
on_x8:=0; on_sep8:=0; on_lhcb :=0;

option, warn, -echo, info; ! verify

!Repeat error assignment washed out by "use"
call, file="ti8_repo/mbi_b3_error.madx";

select,  flag=twiss,clear;
select, flag=twiss, column=NAME, KEYWORD, S, L, TILT, KICK, HKICK, VKICK, ANGLE, K0L, K0SL, K1L, K1SL, K2L, K2SL, K3L, K3SL, BETX, BETY, X, PX, Y, PY, DX, DPX, DY, DPY, ALFX, ALFY, MUX, MUY;
set,    format="27.17f";
savebeta, label=bumped, place = "#s";
Twiss, sequence=ti8lhcb2, beta0=INITBETA0, file="twiss_ti8_lhc_q20_nom.tfs";

/***********************************
* Save sequence for JMAD
***********************************/

call, file = "extr_macros/fast_extraction_macros.cmdx";

! Save sequence for JMAD
option, -warn;
exec, save_stitched_sequence(ti8lhcb2, q20);
option, warn;

/***********************************
* Cleaning up
***********************************/

system, "rm ti8_line_repo";
system, "rm -rf lhc_repo";
system, "rm ti8_repo";
system, "rm extr_macros";