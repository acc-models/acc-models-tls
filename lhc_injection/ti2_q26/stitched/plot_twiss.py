import matplotlib.pyplot as plt
import numpy as np
import accphylib.acc_library as al
from cpymad import madx

info, twiss = al.readtfs("./twiss_ti2_lhc_q26_nom.tfs")

plt.figure(figsize=(6, 4))

plt.subplot(211)
plt.plot(twiss.S, twiss.BETX, label="X")
plt.plot(twiss.S, twiss.BETY, label="Y")
plt.xlabel("s / m")
plt.ylabel("betas / m")

plt.subplot(212)
plt.plot(twiss.S, twiss.DX)
plt.plot(twiss.S, twiss.DY)

plt.ylabel("D / m")

plt.figure()
plt.plot(twiss.S, twiss.X)
plt.plot(twiss.S, twiss.Y)

plt.show()

madx = madx.Madx()
madx.call("jmad/")
