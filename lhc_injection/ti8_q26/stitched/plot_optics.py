import matplotlib.pyplot as plt
import numpy as np
import accphylib.acc_library as al

disp_data_ti8_q20 = al.read_disp_yasp(
    "/home/fvelotti/cernbox/data_from_afs/TI8_ring.tfs",
    header=30,
    footer=329 - 276,
)

disp_data_ti8_q20 = disp_data_ti8_q20.drop_duplicates()

h_mask = disp_data_ti8_q20.PLANE == "h"
v_mask = disp_data_ti8_q20.PLANE == "v"

bps_h = disp_data_ti8_q20.index[h_mask]
bps_v = disp_data_ti8_q20.index[v_mask]

info, twiss = al.readtfs("./twiss_ti8_lhc_q20_nom.tfs")
info, twiss_nom = al.readtfs(
    "/home/fvelotti/Downloads/twiss_ti8_lhc_q20_nom.tfs"
)

bps_h_u = list(map(lambda x: x.upper(), bps_h))
bps_v_u = list(map(lambda x: x.upper(), bps_v))

plt.figure(figsize=(6, 4))

plt.subplot(211)
plt.plot(twiss.S, twiss.BETX, label="stitched")
plt.plot(twiss_nom.S, twiss_nom.BETX, label="nom")
plt.xlabel("s / m")
plt.ylabel("betas / m")

plt.subplot(212)
plt.plot(twiss.S, twiss.DX, label="stitched")
plt.plot(twiss_nom.S, twiss_nom.DX, label="nom")
plt.plot(
    twiss_nom.loc[bps_h_u].S,
    disp_data_ti8_q20["DISP"][h_mask][bps_h],
    "o",
    label="data",
)
# plt.plot(twiss.S, twiss.DY)

plt.legend()
plt.ylabel("D / m")

plt.figure()
plt.subplot(211)
plt.plot(twiss.S, twiss.BETY, label="stitched")
plt.plot(twiss_nom.S, twiss_nom.BETY, label="nom")

plt.subplot(212)
plt.plot(twiss.S, twiss.DY)
plt.plot(twiss_nom.S, twiss_nom.DY)
plt.plot(
    twiss_nom.loc[bps_v_u].S,
    disp_data_ti8_q20["DISP"][v_mask][bps_v],
    "o",
    label="data",
)

plt.legend()
plt.show()
