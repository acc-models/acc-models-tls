!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Sequence for LHC injection stitched model for TI2
! F.M. Velotti, M. Fraser
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

option, RBARC=FALSE;
option, echo;

title,   "TI2 line for LHC";
/***************************************
* Cleaning .tfs output files
***************************************/


/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../../sps_extraction/tt60ti2 ti2_repo";
system, "ln -fns ./../../../sps_extraction/tt60ti2_q20/line ti2_line_repo";
system, "ln -fns ./../../../sps_extraction/sps_ext_elements extr_macros";

set, format="22.10e";
option, echo, warn, info;

/***************************************
* Load model
***************************************/

call,    file = "ti2_repo/ti2.seq";
call,    file = "ti2_repo/ti2_apertures.dbx";
call,    file = "ti2_line_repo/ti2_liu.str";

! Load LHC sequnce
system,"[ ! -e lhc_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-lhc -b 2022 lhc_repo";

option,  echo, warn, info;

beam,    sequence=ti2, particle=proton, pc= 450;
use,     sequence=ti2;

! Errors on b2 and b3 as measured
call, file="ti2_repo/mbi_b3_error.madx";

! Using original initial conditions as used for matching
call, file = "ti2_line_repo/../stitched/sps_tt60_ti2_lhc_q20.inp";

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=Y0,
      PY=PY0;

};

exec, set_ini_conditions();

show, INITBETA0;

select, flag=twiss, column=NAME, KEYWORD, S, L, TILT, KICK, HKICK, VKICK, ANGLE, K0L, K0SL, K1L, K1SL, K2L, K2SL, K3L, K3SL, BETX, BETY, X, PX, Y, PY, DX, DPX, DY, DPY, ALFX, ALFY, MUX, MUY;
set, format="22.10e";
Twiss,file="./twiss_ti2.tfs",beta0=INITBETA0, x=x0,px=xp0;


/***************************************
* Adding LHC
***************************************/

option, -warn;
call, file = "./lhc_b1_complete.seq";
option, warn;

Linjdsr3 = 26658.8832; Lti2 = 3188.384282;

ti2lhcb1 : sequence, refer=entry, l= Linjdsr3 + Lti2 ;
TI2, at=0;
lhcb1, at=0, from=lhcinj.ti2;
endsequence;


beam,    sequence=ti2lhcb1, particle=proton, pc= 450;
use,     sequence=ti2lhcb1;

on_x1:=0; on_sep1:=0;
on_x2:=0; on_sep2:=0; on_alice:=0;
on_x5:=0; on_sep5:=0;
on_x8:=0; on_sep8:=0; on_lhcb :=0;

option, warn, -echo, info; ! verify

!Repeat error assignment washed out by "use"
call, file="ti2_repo/mbi_b3_error.madx";

select,  flag=twiss,clear;
select, flag=twiss, column=NAME, KEYWORD, S, L, TILT, KICK, HKICK, VKICK, ANGLE, K0L, K0SL, K1L, K1SL, K2L, K2SL, K3L, K3SL, BETX, BETY, X, PX, Y, PY, DX, DPX, DY, DPY, ALFX, ALFY, MUX, MUY;
set,    format="27.17f";
savebeta, label=bumped, place = "#s";
Twiss, sequence=ti2lhcb1, beta0=INITBETA0, file="twiss_ti2_lhc_complete_q20.tfs";


/***********************************
* Cleaning up
***********************************/

system, "rm ti2_line_repo";
system, "rm -rf lhc_repo";
system, "rm ti2_repo";
system, "rm extr_macros";