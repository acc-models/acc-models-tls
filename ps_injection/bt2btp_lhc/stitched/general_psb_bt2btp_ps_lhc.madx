!==============================================================================================
! MADX file for PSB-BT2-BTP-PS LHC optics
!
! S. Ogur, W. Bartmann, M.A. Fraser, F.M. Velotti
!==============================================================================================
option, echo;

title, "PSB/BT2/BTP/PS optics";

/***************************************
* Load needed repos
***************************************/

system,"[ -d /afs/cern.ch/eng/acc-models/ps/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/ps/2021 ps_repo";
system,"[ ! -e ps_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-ps -b 2021 ps_repo";

stitchps = 1;
call, file ='./../../../psb_extraction/bt2btp_lhc/stitched/general_psb_bt2btp_lhc.madx';

/*****************************************************************************
 Calculate initial conditions for PS injection
*****************************************************************************/

call, file = "./../../ps/load_ps_injection_lhc.madx";

exec, calculate_injection(4.3e-3);

SEQEDIT, SEQUENCE=PS;
       	CYCLE, START=PI.SMH42.ENDMARKER; 
        FLATTEN;
ENDEDIT;

use, sequence=ps;
select, flag=twiss, column=name,keyword,l,s,K0L,K1L,K2L,K3L,x,px,y,py,betx,alfx,bety,alfy,dx,dpx,dy,dpy,mux,muy,angle,K0L,K1L,K2L,K2S,K3L,K3S,VKICK,HKICK;
twiss, sequence=ps, centre=true, beta0 = BETA0INJ;


/*******************************************************************************
  Set nominal initial beam parameters at PS injection (reference frame modified) 
 *******************************************************************************/

call, file = "./../../../psb_extraction/bt2btp_lhc/stitched/ps2_start_lhc.inp";
exec, set_ini_conditions();
INITBETA0->x = x0 + xpsinj;
INITBETA0->px = px0 + pxpsinj;
INITBETA0->dpx = dpx0 + pxpsinj/(beam->beta);

/*******************************************************************************
 * Run twiss for BT2-BTP-PS and stitch result for nominal case
 *******************************************************************************/

SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, sequence=ps, beta0 = INITBETA0;

! Make one single tfs file for both rings and BT-BTP transfer line
len_twiss_ps = table(twiss, tablelength);

i = 1; ! Set index to 1 here because the sequence was cycled and not arbitrary $start marker is present on row = 1
option, -info;
while(i < len_twiss_ps){

    if(i == 1){
        SETVARS, TABLE=nominal, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=nominal;

    i = i + 1;
};
option, info;
write, table=nominal, file="twiss_psb_bt2btp_ps_lhc_nom_complete.tfs";

/*******************************************************************************
  Set kick response initial beam parameters at PS injection (reference frame modified) 
 *******************************************************************************/
call, file = "./../../../psb_extraction/bt2btp_lhc/stitched/ps2_start_kick_response_lhc.inp";
exec, set_ini_conditions();
INITBETA0->x = x0 + xpsinj;
INITBETA0->px = px0 + pxpsinj;
INITBETA0->dpx = dpx0 + pxpsinj/(beam->beta);

/*******************************************************************************
 * Run twiss for BT2-BTP-PS and stitch result for nominal case
 *******************************************************************************/

SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, sequence=ps, beta0 = INITBETA0;

! Make one single tfs file for both rings and BT-BTP transfer line
len_twiss_ps = table(twiss, tablelength);

i = 1; ! Set index to 1 here because the sequence was cycled and not arbitrary $start marker is present on row = 1
option, -info;
while(i < len_twiss_ps){

    if(i == 1){
        SETVARS, TABLE=trajectory, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=trajectory;

    i = i + 1;
};
option, info;
write, table=trajectory, file="twiss_psb_bt2btp_ps_lhc_kick_response_complete.tfs";

/***********************************************************
* JMAD: prepare single stitched sequences
************************************************************/

/***********************************************************
* PI.SMH42 removed from PS sequence (avoid YASP reading
* septum at end of stitched model)
* PI.SMH42.BTP renamed to PI.SMH42 in BTP sequence (otherwise
* YASP doesn't recognise the septum name)
************************************************************/

SEQEDIT, SEQUENCE=ps;
remove,element=PI.SMH42;
ENDEDIT;

PI.SMH42: rbend,l=0.942,angle:= angleSMH42;

SEQEDIT, SEQUENCE=btp;
REPLACE, ELEMENT=PI.SMH42.btp, BY=PI.SMH42;
ENDEDIT;

EXTRACT, SEQUENCE=psb2, FROM=PSB2.START, TO=BR2.BT_START, NEWNAME=psb2_ext;

psbbt2btpps: SEQUENCE, refer=ENTRY, L  = lpsbext + lbt2 + lbtp + lps;
    psb2_ext        , AT =  0.0000000000 ;
	bt2          	, AT =  lpsbext ;
    btp          	, AT =  lpsbext + lbt2;
    ps              , AT =  lpsbext + lbt2 + lbtp;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = psbbt2btpps;
FLATTEN;
ENDEDIT;

! Matrix to correct for TL reference frame shift
lm1: MATRIX, L=0,  kick1=-xtl, kick2=-pxtl, rm26=-pxtl/(beam->beta), rm51=pxtl/(beam->beta);

! Matrix to correct for TL reference frame shift (BTP to PS)
lm2: MATRIX, L=0,  kick1=xpsinj, kick2=pxpsinj, rm26=pxpsinj/(beam->beta), rm51=-pxpsinj/(beam->beta);

SEQEDIT, SEQUENCE = psbbt2btpps;
FLATTEN;
INSTALL, ELEMENT = lm1, at = 0, from=BR2.BT_START;
INSTALL, ELEMENT = lm2, at = 0, from=PI.SMH42.ENDMARKER;
FLATTEN;
ENDEDIT;

bt2btpps: SEQUENCE, refer=ENTRY, L  =  lbt2 + lbtp + lps;
	bt2          	, AT =  0.0 ;
    btp          	, AT =  lbt2;
    ps              , AT =  lbt2 + lbtp ;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = bt2btpps;
FLATTEN;
ENDEDIT;

SEQEDIT, SEQUENCE = bt2btpps;
FLATTEN;
INSTALL, ELEMENT = lm2, at = 0, from=PI.SMH42.ENDMARKER;
FLATTEN;
ENDEDIT;

! Special sequence for HE corrector knob upstream up BT.SMH15

EXTRACT, SEQUENCE=psb2, FROM=BR2.DVT14L1, TO=BR2.BT_START, NEWNAME=smh_ext;

lsmh_ext = lpsbext - 127.93409;

smhbt2btpps: SEQUENCE, refer=ENTRY, L  = lsmh_ext + lbt2 + lbtp + lps;
    smh_ext        , AT =  0.0000000000 ;
	bt2          	, AT =  lsmh_ext ;
    btp          	, AT =  lsmh_ext + lbt2;
    ps              , AT =  lsmh_ext + lbt2 + lbtp;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = smhbt2btpps;
FLATTEN;
ENDEDIT;

SEQEDIT, SEQUENCE = smhbt2btpps;
FLATTEN;
INSTALL, ELEMENT = lm1, at = 0, from=BR2.BT_START;
INSTALL, ELEMENT = lm2, at = 0, from=PI.SMH42.ENDMARKER;
FLATTEN;
ENDEDIT;

! Ensure the kick response is OFF
kBE2KFA14L1 := kBE2KFA14L1REF;

 set, format="22.10e";
use, sequence= psbbt2btpps; 
option, -warn;
save, sequence=psbbt2btpps, beam, file='jmad/psbbt2btpps_lhc.jmad';
option, warn;

 set, format="22.10e";
use, sequence= smhbt2btpps; 
option, -warn;
save, sequence=smhbt2btpps, beam, file='jmad/smhbt2btpps_lhc.jmad';
option, warn;

 set, format="22.10e";
use, sequence= bt2btpps;  
option, -warn;
save, sequence=bt2btpps, beam, file='jmad/bt2btpps_lhc.jmad';
option, warn;

/***************************************
* Copy intial conditions to jmad folder
***************************************/

system, "cp psb2_start_lhc.inp jmad";
system, "cp smh2_start_lhc.inp jmad";
system, "cp bt2_start_lhc.inp jmad";

/*************************************
* Cleaning up
*************************************/

system, "rm twiss_psb_bt2btp_lhc_nom_complete.tfs";
system, "rm twiss_psb_bt2btp_lhc_kick_response_complete.tfs";

system, "rm bt_repo";
system, "rm btp_repo";
system, "rm -rf ps_repo || rm ps_repo";
system, "rm -rf psb_repo || rm psb_repo";


stop;
