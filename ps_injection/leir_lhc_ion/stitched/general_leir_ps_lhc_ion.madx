!==============================================================================================
! MADX file for PSB-BT3-BTP-PS LHC optics
!
!==============================================================================================
option, echo;

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";
system, "rm ./jmad/*";

/***************************************
* Load needed repos
***************************************/

system,"[ -d /afs/cern.ch/eng/acc-models/ps/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/ps/2021 ps_repo";
system,"[ ! -e ps_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-ps --depth=1 -b 2021 ps_repo";

system, "ln -nfs ./../../../leir_extraction/ee_etp/line/jmad ee_etp_repo";

/***************************************
* EE+ETP TL optics
***************************************/

call, file = "./ee_etp_repo/leir_extraction.inp";
call, file = "./ee_etp_repo/beta0.inp";
call, file = "./ee_etp_repo/leir_extraction_savedseq.seq";

use, sequence=eeetl;
select, flag=twiss, column=name,keyword,l,s,K0L,K1L,K2L,K3L,x,px,y,py,betx,alfx,bety,alfy,dx,dpx,dy,dpy,mux,muy,angle,K0L,K1L,K2L,K2S,K3L,K3S,VKICK,HKICK;
twiss, centre=true, beta0 = initbeta0;

l_eetl = table(twiss, eeetl$end, s);

/*****************************************************************************
 Calculate initial conditions for PS injection
*****************************************************************************/

call, file = "./../../ps/load_ps_injection_ions.madx";

exec, calculate_injection(7.7e-3);
    use, sequence=psinj;
    select, flag=twiss, column=name,keyword,l,s,K0L,K1L,K2L,K3L,x,px,y,py,betx,alfx,bety,alfy,dx,dpx,dy,dpy,mux,muy,angle,K0L,K1L,K2L,K2S,K3L,K3S,VKICK,HKICK;
    twiss, sequence=psinj, centre=true, beta0 = BETA0INV;

SEQEDIT, SEQUENCE=PS;
       	CYCLE, START=PI.SMH26.ENDMARKER;
        FLATTEN;
ENDEDIT;

use, sequence=ps;
select, flag=twiss, column=name,keyword,l,s,K0L,K1L,K2L,K3L,x,px,y,py,betx,alfx,bety,alfy,dx,dpx,dy,dpy,mux,muy,angle,K0L,K1L,K2L,K2S,K3L,K3S,VKICK,HKICK;
twiss, sequence=ps, centre=true, beta0 = BETA0INJ;


l_ps = table(twiss, pspi.smh26.endmarker_p_, s);

/***********************************************************
* JMAD: prepare single stitched sequences
************************************************************/

/***********************************************************
* PI.SMH42 removed from PS sequence (avoid YASP reading
* septum at end of stitched model)
* PI.SMH42.BTP renamed to PI.SMH42 in BTP sequence (otherwise
* YASP doesn't recognise the septum name)
************************************************************/


eeetlps: SEQUENCE, refer=ENTRY, L  = l_eetl + l_ps;
    eeetl        , AT =  0.0000000000 ;
    ps           , AT =  l_eetl;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = eeetlps;
FLATTEN;
ENDEDIT;



! Matrix to correct for TL reference frame shift (BTP to PS)
lm2: MATRIX, L=0,  kick1=xpsinj, kick2=pxpsinj, rm26=pxpsinj/(beam->beta), rm51=-pxpsinj/(beam->beta);

SEQEDIT, SEQUENCE = eeetlps;
FLATTEN;
INSTALL, ELEMENT = lm2, at = 0, from=PI.SMH26.ENDMARKER;
FLATTEN;
ENDEDIT;


use, sequence=eeetlps;

SEQEDIT, SEQUENCE = eeetlps;
FLATTEN;
REMOVE, ELEMENT = PI.SMH26.ENDMARKER;
FLATTEN;
ENDEDIT;

use, sequence=eeetlps;
select, flag=twiss, column=name,keyword,l,s,K0L,K1L,K2L,K3L,x,px,y,py,betx,alfx,bety,alfy,dx,dpx,dy,dpy,mux,muy,angle,K0L,K1L,K2L,K2S,K3L,K3S,VKICK,HKICK;
twiss, centre=true, beta0 = initbeta0, file="twiss_eeetl_ps_lhc_ion_nom.tfs";


/***************************************
* Copy intial conditions and sequence to jmad folder
***************************************/

set, format="22.10e";
use, sequence= eeetlps;
option, -warn;
save, sequence=eeetlps, beam, file="jmad/eeetl_ps_lhc_ion.jmad";
option, warn;

/***********************************************
* Save initial parameters to file for TL usage
***********************************************/
assign, echo="./jmad/eeetl_ps_lhc_ion.inp";

betx0 = initbeta0->betx;
bety0 = initbeta0->bety;
alfx0 = initbeta0->alfx;
alfy0 = initbeta0->alfy;

dx0 = initbeta0->dx;
dy0 = initbeta0->dy;

dpx0 = initbeta0->dpx;
dpy0 = initbeta0->dpy;

print, text="/*********************************************************************************";
print, text='Initial conditions from MADX stitched model of EEETL and PS for LHC ION beam';
print, text="*********************************************************************************/";

print, text = '';
value,betx0;
value,bety0;

value,alfx0;
value,alfy0;

value,dx0 ;
value,dy0 ;

value,dpx0;
value,dpy0;

assign, echo=terminal;



/*************************************
* Cleaning up
*************************************/

system, "rm -rf ps_repo || rm ps_repo";
system, "rm ee_etp_repo";

