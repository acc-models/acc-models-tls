!==============================================================================================
! MADX file for LNE51 optics
!
! M.A. Fraser, F.M. Velotti
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "ELENA/LNE51 optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Cleaning .inp output files
***************************************/

system, "rm *.inp";

/***************************************
* Load needed repos
***************************************/
system, "ln -fns ../../../../acc-models-elena elena_repo";
system, "ln -fns ./../../lne lne_repo";


/*******************************************************************************
 * beam
 *******************************************************************************/
 beam, particle=antiproton;

 mass=beam->mass;

 Ekin=0.0001; ! 100 Kev

 gamman=(Ekin/mass)+1;
 beta=sqrt(-((1/gamman)^2)+1);
 value,beta;
 pcn=sqrt((mass^2)*((gamman^2)-1));
 
 beam, particle=antiproton,pc=pcn,exn=6E-6/6,eyn=4E-6/6;

/*****************************************************************************
 Calculate live initial condition for KR or any other changes in the ring
  - For now only macro to evaluate changes in ZDFL.0610
*****************************************************************************/

call, file = "lne_repo/load_elena_extraction50.madx";

! It needs as input as ZDFL.0610 delta kick (absolute value in rad), sign and 
! file name to save the ring twiss 
! Returns all initial conditions needed, hence a set_ini_conditions() is needed 
! if "sign" (second argument) = 1, positive kick, negative otherwise

 set, format="22.10e";
exec, calculate_extraction(0e-3, 1, twiss_elena_stitched.tfs);

exec, set_ini_conditions();

/***********************************************
* Save initial parameters to file for TL usage
***********************************************/

exec, write_ini_conditions(0,0,tl_initial_cond,lne50_start.inp);

/*****************************************************************************
 * Load element R matrix definition
 *****************************************************************************/
 call, file = "lne_repo/deflectors.ele";


/*****************************************************************************
 * LNE50
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "lne_repo/lne50/lne50.ele";
 call, file = "lne_repo/lne50/lne50_k.str";
 call, file = "lne_repo/lne50/lne50.seq";
 !call, file = "lne_repo/lne50/lne50.dbx"; !Presently no aperture database: to be updated
 option, echo;

 EXTRACT, SEQUENCE=lne50, FROM=lne.start.5000, TO=lne.lne50.lne51, NEWNAME=lne50to51;

/*******************************************************************************
 * LNE51 line
 *******************************************************************************/
 call, file = "lne_repo/lne51/lne51.ele";
 call, file = "lne_repo/lne51/lne51_k.str";
 call, file = "lne_repo/lne51/lne51.seq";
 !call, file = "lne_repo/lne51/lne51.dbx"; !Presently no aperture database: to be updated
 

/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/
lne50lne51: sequence, refer=ENTRY, l = 3.7191517+7.13777336;
   lne50to51              , at =        0;
   lne51                  , at = 3.7191517;
  endsequence;
  
 SEQEDIT, SEQUENCE=lne50lne51; FLATTEN; ENDEDIT;

/*******************************************************************************
 * Run twiss for LNE51 and stitch result
 *******************************************************************************/

use, sequence= lne50lne51;  
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta, label=lne51_start, place=lne.lne50.lne51;
twiss, beta0=initbeta0, file = "twiss_lne50_lne51.tfs";

! Make one single tfs file for both ring and FTN transfer line using kickers
len_twiss_tl = table(twiss, tablelength);

i = 2;
option, -info;
while(i < len_twiss_tl){

    if(i == 2){
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=trajectory;

    i = i + 1;
};
option, info;
write, table=trajectory, file="twiss_lnr_lne50_lne51_nom_complete.tfs";

/***********************************************
* Save initial parameters to file for TL usage
***********************************************/

exec, write_ini_conditions(0,0,lne51_start,lne51_start.inp);

/***********************************************************
* JMAD: prepare single sequences
************************************************************/

llnr = 2.8422112780e+01;
llne50lne51 = 10.85692506;

lnrlne50lne51: SEQUENCE, refer=ENTRY, L  = llnr + llne50lne51;
    ELENA_EXTRACT, AT =  0.0000000000 ;
    lne50lne51 , AT =  llnr ;
ENDSEQUENCE;

! Ensure the kick response is OFF
LNR.ZDFHL.0610, kick = 0;

 set, format="22.10e";
use, sequence= lnrlne50lne51;  
option, -warn;
save, sequence=lnrlne50lne51, beam, file='jmad/lnrlne50lne51.jmad';
option, warn;

 set, format="22.10e";
use, sequence= lne51; 
option, -warn;
save, sequence=lne51, beam, file='jmad/lne51.jmad';
option, warn;

/***************************************
* Copy intial conditions to jmad folder
***************************************/

system, "cp lnr_start.inp jmad";
system, "cp lne51_start.inp jmad";

/*************************************
* Cleaning up
*************************************/

system, "rm lne_repo";
system, "rm elena_repo";

stop;
