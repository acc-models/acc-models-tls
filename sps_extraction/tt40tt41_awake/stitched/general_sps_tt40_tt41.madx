!====================================================
! MADX model of SPS-TT40-TT41 for AWAKE (Q20) optics
!
! F.M.Velotti: Using operational optics and settings 
!              to make stitched model
!====================================================
 title, "SPS-TT40-TT41 AWAKE (Q20) optics. Protons - 400 GeV/c";

 option, echo;
 option, RBARC=FALSE;

  set, format="22.10e";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";
system, "rm ./jmad/*";

/***************************************
* Load needed repos
***************************************/

/* system,"[ -d /afs/cern.ch/eng/acc-models/sps/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/sps/2021 sps_repo"; */
system,"[ ! -e sps_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-sps -b 2021 sps_repo";

system, "ln -fns ./../../sps_ext_elements sps_extr_repo";

system, "ln -fns ./../line tt41_line_repo";

/***************************************
* TT40-TT41 line model
***************************************/

option, -warn;
    call, file = "tt41_line_repo/tt40tt41_awake.seq";
option, warn;

! There are other srtengths files, but these are only for special runs or MD
! the changes are done using a knob directly in trim editor 
call, file = "tt41_line_repo/str/tt40tt41_strength.str";

beam,    particle=proton, pc= 400;
use,     sequence=tt40tt41;

! Apparently there is an issue with survey - need to extract sequence
! to make proper stitched model

EXTRACT, sequence=tt40tt41, FROM=pt.extraction, TO=T.40, newname=tt40_tt41;

use, sequence = tt40_tt41;

ex_g = beam->ex;
ey_g = beam->ey;
dpp = 1.5e-3;

mvar1:= sqrt(table(twiss, betx) * ex_g + (table(twiss, dx) * dpp)^2) * 1e3;
mvar2 := sqrt(table(twiss, bety) * ey_g + (table(twiss, dy) * dpp)^2) * 1e3;

call, file = "./../stitched/tt40tt41_nominal.inp";

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=Y0,
      PY=PY0;

};

exec, set_ini_conditions();

select, flag = twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file="twiss_tt40tt41.tfs";

l_tt40_tt41 = table(twiss, T.40, S);
/*****************************************************************************
Calculate extraction from SPS and prepare for stitching
*****************************************************************************/

call, file = "./load_awake_extraction.madx";

! Make stitched model of SPS extraction
exec, calculate_stitched_absolute_trajecotry(twiss_sps_tt40tt41_nom.tfs, tt40_tt41);

! Save sequence for JMAD
exec, save_stitched_sequence(sps_tt40_tt41, q20);

/************************************
* Cleaning up
************************************/

system, "rm -rf sps_repo || rm sps_repo";
system, "rm tt41_line_repo";
system, "rm sps_extr_repo";

