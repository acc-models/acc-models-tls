!====================================================
! Load SPS extraction via LSS4 for Q20 optics
!
! F.M. Velotti
!
!====================================================

option, RBARC=FALSE;

/******************************************************************
 * Call lattice files
 ******************************************************************/
option, -warn;
call,file="sps_repo/sps.seq";
call,file="sps_repo/strengths/lhc_q20.str";

call, file="sps_extr_repo/fast_extraction_macros.cmdx";
option, warn;

/*******************************************************************************
 * Beam
 *******************************************************************************/
Beam, particle=PROTON,pc=450,exn=3.5e-6,eyn=3.5E-6;
BRHO      := BEAM->PC * 3.3356;

ex_g = beam->ex;
ey_g = beam->ey;
dpp = 1.5e-3;

mvar1:= sqrt(table(twiss, betx) * ex_g + (table(twiss, dx) * dpp)^2) * 1e3;
mvar2 := sqrt(table(twiss, bety) * ey_g + (table(twiss, dy) * dpp)^2) * 1e3;

/******************************************************************
 * Twiss with bump on
 ******************************************************************/
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26, mvar1, mvar2;

use, sequence=SPS;

exec, install_extraction_points();

! Activating bumps at 35.2 mm in LSS4
exec, bumps_on_q20(35.2, 0.0);

! Cycle machine to end at extraction point in LSS4
SEQEDIT, sequence=SPS;
CYCLE, START=EXTRPT_4_start;
FLATTEN;
ENDEDIT;

use, sequence = sps;

savebeta, label=bumped, place = EXTRPT_4_START;
twiss;

! MKE voltage as scaling from LHC nominal
mke4_nom = 27.73; !kV
kmse418 = 0.002009198;

exec, set_kicker_lss4_q20(mke4_nom);

exec, replace_quad(qda.41910, qda.419.coil);
use, sequence = sps;
exec, make_quad_coil(qda.419.coil);

savebeta,label=initial_cond, place = EXTRPT_4;
twiss, beta0=bumped;


calculate_stitched_absolute_trajecotry(__file_name__, __seq_name__) : macro = {


    x_inj  = -1 * table(twiss, EXTRPT_4, x);
    px_inj = -1 * table(twiss, EXTRPT_4, px);

    value, x_inj, px_inj;

    change_ref: MATRIX, L=0,  kick1 = x_inj, kick2 = px_inj, rm26= px_inj/(beam->beta), rm51 = -px_inj/(beam->beta);

    seqedit, sequence = sps;
        install, element = change_ref, at = 1e-10, from = EXTRPT_4;
        flatten;
    endedit;

    use, sequence = sps;

    exec, make_quad_coil(qda.419.coil);
    twiss, beta0 = bumped;

    l_sps = table(twiss, EXTRPT_4, S);

    value, l_sps, l___seq_name__, l_sps + l___seq_name__;

    sps___seq_name__: sequence, refer=entry, l = l_sps + l___seq_name__;
        sps, at = 0.0;
        __seq_name__, at = 0.0, from=EXTRPT_4;
    endsequence;

    use, sequence = sps___seq_name__;

    exec, make_quad_coil(qda.419.coil);
    twiss, beta0 = bumped, file="__file_name__";


};



