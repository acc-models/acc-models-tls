!==============================================================================================
! MADX file for TT25-T6 SFTION low-energy optics ** this is not used in real operation as usually no beam on T6 **
!
! F.M. Velotti
! based on version from AFS repository and from LSA database
!
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "TT25/T6 SFTION optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../tt25t6 tt25_repo";

/***************************************
* Make model
***************************************/

call, file = "tt25_repo/tt25t6.seq";
call, file = "./t6_pb_low_ht.str";


/*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
 call, file = "./tt25initial_conditions.inp";


/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;

! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();


/*******************************************************************************
 * beam
 *******************************************************************************/

Beam, particle=proton, pc=400;


/*******************************************************************************
 * twiss
 *******************************************************************************/

use, sequence = tt25t6;

select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "./twiss_tt25t6_sftion_low_ht_nom.tfs";

/***********************************
* Cleaning up
***********************************/
system, "rm tt25_repo";

stop;
