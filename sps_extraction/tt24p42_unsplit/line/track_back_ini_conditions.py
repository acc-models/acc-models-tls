import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from cpymad.madx import Madx

madx = Madx()

GENERAL = "general_tt24p42_unsplit.madx"

madx.call(file=GENERAL, chdir=True)

madx.use(sequence="tt24t4")
twiss = madx.twiss(beta0="initbeta0_pre").dframe()

twiss_back = madx.twiss(beta0="initbeta0").dframe()

fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(twiss.s, twiss.betx, label="original")
axes[0].plot(twiss_back.s, twiss_back.betx, c="C2", label="tracked")
axes[0].set(ylabel=r"$\beta_x$ / m", xlabel="s / m")
axes[0].legend()

axes[1].plot(twiss.s, twiss.bety, label="original")
axes[1].plot(twiss_back.s, twiss_back.bety, c="C2", label="tracked")
axes[1].set(ylabel=r"$\beta_y$ / m", xlabel="s / m")
axes[1].legend()
plt.show()
