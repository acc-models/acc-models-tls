!==============================================================================================
! MADX file for TT20-T2 SFTION optics
!
! F.M. Velotti
! based on version from AFS repository and from LSA database
!
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "TT20/T2 SFTION optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../tt20t2 tt20_repo";
system, "ln -fns ./../../tt20_utils tt20_utils_repo";
system, "ln -fns ./../../sps_ext_elements sps_ext_repo";
system, "ln -fns ./../../tt24t4_sftion_low_high_transmission/line tt24t4_repo";

/***************************************
* Make model
***************************************/

call, file = "tt20_repo/tt20t2.seq";
call, file = "./t2_ions_le_ht.str";

call, file = "sps_ext_repo/slow_extraction_macros.cmdx";

/*******************************************************************************
! set initial twiss parameters - initial conditions from tracking in the SPS
! DY has been measured in TT20 and not 0 at extraction - ignored for this
! set of initial conditins. Please refer to the *.inp file for more details
 *******************************************************************************/
 call, file = "./tt20_initial_conditions.inp";

/*******************************************************************************
 * beam
 *******************************************************************************/

Beam, particle=proton, pc=30;


/*******************************************************************************
 * twiss
 *******************************************************************************/

use, sequence = tt20t2;

select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0_tt20_tracking, file = "./twiss_tt20t2_sftion_low_high_transmission_nom.tfs";

/*******************************************************************************
 * twiss with correct optics after splitters using P. Arrutia tracking results
 *******************************************************************************/

! Start TT20 to start 1st splitter
use, sequence = tt20t2, range=#s/ENDTT21;

select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta, label=bumped, place=#s;
twiss, beta0=initbeta0_tt20_tracking;


! End 1st splitter to start 2nd splitter
call, file = "./tt22_initial_conditions.inp";
use, sequence = tt20t2, range=BEGTT22/ENDTT22;

select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0_tt22;

! End 2nd splitter to T2
call, file = "./tt23_initial_conditions.inp";
use, sequence = tt20t2, range=BEGTT23/ENDTT23;

select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0_tt23;


/***********************************
* Stitch to targets with fake optics (just for kick response)
* it does not produce any output - either save twiss files or
* use cpymad
***********************************/
call, file = "tt20_utils_repo/split_tt20.madx";
call, file = "tt20_utils_repo/stitch_to_t4.madx";

call, file = "tt24t4_repo/t4_pb_low_ht.str";

twiss, beta0=initbeta0_tt20_tracking;

! Save sequence for JMAD
exec, save_stitched_sequence(tt20t2, sftion_low_ht);
/***********************************
* Cleaning up
***********************************/
system, "rm tt20_repo";
system, "rm tt20_utils_repo";
system, "rm tt24t4_repo";
system, "rm sps_ext_repo";

