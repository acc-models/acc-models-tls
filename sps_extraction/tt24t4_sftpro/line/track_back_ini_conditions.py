import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from cpymad.madx import Madx

madx = Madx()

GENERAL = "general_tt24t4_sftpro.madx"

madx.call(file=GENERAL, chdir=True)

madx.use(sequence="tt24t4")
twiss = madx.twiss(beta0="initbeta0_tt24_tracking").dframe()

betx0 = 1.856207e01
alfx0 = 2.366476e-01
gammax0 = (1 + alfx0**2) / betx0

twiss_x = np.array([betx0, alfx0, gammax0])

dx0 = -2.525527e-01
dpx0 = 5.749611e-02

twiss_dx = np.array([dx0, dpx0])

bety0 = 5.410823e03
alfy0 = -4.285318e-01
gammay0 = (1 + alfy0**2) / bety0

twiss_y = np.array([bety0, alfy0, gammay0])

dy0 = -2.458923e-01
dpy0 = -1.220249e-03

twiss_dy = np.array([dy0, dpy0])

L = -16.365
drift = np.array([[1, L], [0, 1]])

m_matrix = np.array(
    [
        [drift[0, 0] ** 2, -2 * drift[0, 0] * drift[0, 1], drift[0, 1] ** 2],
        [
            -drift[0, 0] * drift[1, 0],
            drift[0, 0] * drift[1, 1] + drift[0, 1] * drift[1, 0],
            -drift[0, 1] * drift[1, 1],
        ],
        [drift[1, 0] ** 2, -2 * drift[1, 0] * drift[1, 1], drift[1, 1] ** 2],
    ]
)

twiss_x1 = np.dot(m_matrix, twiss_x)
twiss_y1 = np.dot(m_matrix, twiss_y)

print(
    f"betx0 = {betx0}, alfx0 = {alfx0}, gammax0 = {gammax0};\nbety0 = {bety0}, alfy0 = {alfy0}, gammay0 = {gammay0}"
)
print(
    f"alfx0 = {twiss_x1[1]};\nbetx0 = {twiss_x1[0]};\nalfy0 = {twiss_y1[1]};\nbety0 = {twiss_y1[0]};"
)

twiss_dx1 = np.dot(drift, twiss_dx)
twiss_dy1 = np.dot(drift, twiss_dy)

print(
    f"dx0 = {twiss_dx1[0]};\ndpx0 = {twiss_dx1[1]};\ndy0 = {twiss_dy1[0]};\ndpy0 = {twiss_dy1[1]};"
)

madx.globals["betx0"] = twiss_x1[0]
madx.globals["alfx0"] = twiss_x1[1]
madx.globals["bety0"] = twiss_y1[0]
madx.globals["alfy0"] = twiss_y1[1]

madx.globals["dx0"] = twiss_dx1[0]
madx.globals["dpx0"] = twiss_dx1[1]

madx.globals["dy0"] = twiss_dy1[0]
madx.globals["dpy0"] = twiss_dy1[1]

change_initbeta = """
INITBETA0_TT24_tracking_back: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  T=0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0,
  X=X0,
  PX=PX0,
  Y=PY0,
  PY=PY0,
  PT=PT0;
"""

madx.input(change_initbeta)

twiss_back = madx.twiss(beta0="initbeta0_tt24_tracking_back").dframe()

fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(twiss.s, twiss.betx, label="original")
axes[0].plot(twiss_back.s, twiss_back.betx, c="C2", label="tracked")
axes[0].set(ylabel=r"$\beta_x$ / m", xlabel="s / m")
axes[0].legend()

axes[1].plot(twiss.s, twiss.bety, label="original")
axes[1].plot(twiss_back.s, twiss_back.bety, c="C2", label="tracked")
axes[1].set(ylabel=r"$\beta_y$ / m", xlabel="s / m")
axes[1].legend()
plt.show()
