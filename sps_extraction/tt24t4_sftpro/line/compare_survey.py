import pandas as pd
import accphylib.acc_library as al
import numpy as np
import matplotlib.pyplot as plt

survey, _ = al.read_tfs("./survey_tt24.tfs")

survey_geode = pd.read_csv(
    "/home/fvelotti/Downloads/Report consultation des points faisceaux.csv",
    encoding="latin1",
)

survey_geode.rename(
    columns={"X (m)": "X", "Y (m)": "Y", "Z (m)": "Z", "Gisement (gon)": "theta_geode"},
    inplace=True,
)

plt.figure()
plt.plot(survey["X"], survey["Y"], label="Survey")
plt.plot(-survey_geode["X"], survey_geode["Z"], label="Geode")
plt.xlabel("$X_{madx}$ / m")
plt.ylabel("$Y_{madx}$ / m")
plt.legend()
plt.show()

plt.figure()
plt.plot(survey["X"], survey["Z"], label="Survey")
plt.plot(-survey_geode["X"], survey_geode["Y"], label="Geode")
plt.xlabel("$X_{madx}$ / m")
plt.ylabel("$Z_{madx}$ / m")
plt.legend()
plt.show()
