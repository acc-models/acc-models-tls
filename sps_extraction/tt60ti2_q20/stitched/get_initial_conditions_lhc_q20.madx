!====================================================
! MADX model to obtain extraction parameters 
! for Q20 optics using 2018 extraction settings
! as in operation
!
! F.M. Velotti
!
!====================================================

option, RBARC=FALSE;

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";
system, "rm sps_tt60_ti2_lhc_q20_from_stitched_kickers.inp";

/***************************************
* Load needed repos
***************************************/

/* system,"[ -d /afs/cern.ch/eng/acc-models/sps/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/sps/2021 sps_repo"; */
system,"[ ! -e sps_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-sps -b 2021 sps_repo";

system, "ln -fns ./../../sps_ext_elements sps_extr_repo"


/******************************************************************
 * Call lattice files
 ******************************************************************/

option, -warn;
call,file="sps_repo/sps.seq";
call,file="sps_repo/strengths/lhc_q20.str";

call, file="sps_extr_repo/fast_extraction_macros.cmdx";
option, warn;

set, format="22.10e";

/*******************************************************************************
 * Beam
 *******************************************************************************/
Beam, particle=PROTON,pc=450,exn=3.5e-6,eyn=3.5E-6;
BRHO      := BEAM->PC * 3.3356;

ex_g = beam->ex;
ey_g = beam->ey;
dpp = 1.5e-3;

mvar1:= sqrt(table(twiss, betx) * ex_g + (table(twiss, dx) * dpp)^2) * 1e3;
mvar2 := sqrt(table(twiss, bety) * ey_g + (table(twiss, dy) * dpp)^2) * 1e3;

/******************************************************************
 * Twiss with bump on
 ******************************************************************/
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26, mvar1, mvar2;

use, sequence=SPS;

exec, install_extraction_points();

! Activating bumps at 35 and 42 mm in LSS4 and LSS6 respectively
exec, bumps_on_q20(35, 42);

! Cycle machine to end at extraction point in LSS6
SEQEDIT, sequence=SPS;
CYCLE, START=EXTRPT_6_start;
FLATTEN;
ENDEDIT;

use, sequence = sps;

savebeta, label=bumped, place = EXTRPT_6_START;
twiss, file="twiss_sps_bumps_q20.tfs";

! MKE4 voltage as chosen during commissioning in 2017 after 
! sc kicker termination (double field for same voltage) and
! hence removal of one kicker

mke6_nom = 33.1; !kV
kmse618 = 1.75599e-3;
kmst617 = 0.53188e-3;

exec, set_kicker_lss6_q20(mke6_nom);

exec, replace_quad(qda.61910, qda.619.coil);
use, sequence = sps;
exec, make_quad_coil(qda.619.coil);

savebeta,label=initial_cond, place = EXTRPT_6;
twiss, beta0=bumped, file="twiss_sps_extr_lss6_q20.tfs";


/***********************************************
* Save initial parameters to file for TL usage
***********************************************/
assign, echo="sps_tt60_ti2_lhc_q20_from_stitched_kickers.inp";

betx0 = initial_cond->betx;
bety0 =  initial_cond->bety;

alfx0 = initial_cond->alfx;
alfy0 = initial_cond->alfy;

dx0 = initial_cond->dx;
dy0 = initial_cond->dy;

x0 = initial_cond->x;
y0 = initial_cond->y;

dpx0 = initial_cond->dpx - initial_cond->px/(beam->beta);
dpy0 = initial_cond->dpy;

px0 = initial_cond->px - initial_cond->px;
py0 = initial_cond->py;

print, text="/*********************************************************************************";
print, text='Initial conditions from MADX stitched model of SPS for LHC Q20 beam';
print, text="*********************************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;

value,x0 ;
value,y0 ;
      
value,dpx0;
value,dpy0;
value,px0;
value,py0;

assign, echo=terminal;

/************************************
* Cleaning up
************************************/
system, "rm sps_repo";
system, "rm -rf sps_repo";
system, "rm sps_extr_repo";









