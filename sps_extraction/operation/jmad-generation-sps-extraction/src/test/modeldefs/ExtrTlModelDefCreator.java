/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package modeldefs;

import static cern.accsoft.steering.jmad.tools.modeldefs.creating.ModelDefinitionCreator.scanDefault;

public class ExtrTlModelDefCreator {
    public static void main(String[] args) {
        scanDefault().and().writeTo("..");
    }
}
