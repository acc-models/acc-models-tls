/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package modeldefs.defs;

import java.util.HashSet;
import java.util.Set;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

public class Tt20T2ModelDefinitionFactory implements ModelDefinitionFactory{
    
    private Set<OpticsDefinition> createOpticsDefinitions() {
        Set<OpticsDefinition> definitionSet = new HashSet<>();
        definitionSet.add(new OpticsDefinitionImpl("TT20T2-SFTPRO-2021v1", new CallableModelFileImpl(
                "operation/beta0.inp", ModelFileLocation.REPOSITORY),new CallableModelFileImpl(
                "tt20t2_sftpro/stitched/jmad/sps_tt20t2sftpro_savedseq.seq", ModelFileLocation.REPOSITORY),
                new CallableModelFileImpl(
                        "tt20t2_sftpro/stitched/jmad/sps_tt20t2_sftpro.inp", ModelFileLocation.REPOSITORY)
                ));
        return definitionSet;
    }
    
    

    @Override
    public JMadModelDefinition create() {
        // TODO Auto-generated method stub
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("TT20T2");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setRepositoryPrefix("..");
        modelDefinition.setModelPathOffsets(offsets);

        for (OpticsDefinition opticsDefinition : createOpticsDefinitions()) {
            modelDefinition.addOpticsDefinition(opticsDefinition);
        }
       
        modelDefinition.setDefaultOpticsDefinition(modelDefinition.getOpticsDefinitions().get(0));

        /*
         * SEQUENCE
         */

       
        SequenceDefinitionImpl tt20t2 = new SequenceDefinitionImpl("sps_tt20t2",null);
        modelDefinition.setDefaultSequenceDefinition(tt20t2);
        RangeDefinitionImpl tt20t2range = new RangeDefinitionImpl(tt20t2, "ALL", createExtrInitialConditions());
        tt20t2.setDefaultRangeDefinition(tt20t2range);

        return modelDefinition;
        
        
      
    }
    
    /**
     * Twiss initial conditions from extraction point
     */
    private final TwissInitialConditionsImpl createExtrInitialConditions() {
        
        
        TwissInitialConditionsImpl twissInitialConditions = new TwissInitialConditionsImpl("extrsps-twiss");
        
        twissInitialConditions.setSaveBetaName("EXTR.INITBETA0");
       
        twissInitialConditions.setCalcAtCenter(true);
        twissInitialConditions.setClosedOrbit(false);
      
        return twissInitialConditions;

    }

}
