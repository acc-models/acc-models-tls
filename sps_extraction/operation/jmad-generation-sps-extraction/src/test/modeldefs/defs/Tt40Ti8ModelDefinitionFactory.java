/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package modeldefs.defs;

import java.util.HashSet;
import java.util.Set;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.file.TableModelFileImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

public class Tt40Ti8ModelDefinitionFactory implements ModelDefinitionFactory{
    
    private Set<OpticsDefinition> createOpticsDefinitions() {
        Set<OpticsDefinition> definitionSet = new HashSet<>();
        definitionSet.add(new OpticsDefinitionImpl("TT40TI8-LHC-Q20-2021v1", new CallableModelFileImpl(
                "operation/beta0.inp", ModelFileLocation.REPOSITORY),new CallableModelFileImpl(
                "tt40ti8_q20/stitched/jmad/sps_ti8q20_savedseq.seq", ModelFileLocation.REPOSITORY),
                new CallableModelFileImpl(
                        "tt40ti8_q20/stitched/jmad/sps_ti8_q20.inp", ModelFileLocation.REPOSITORY),
                new TableModelFileImpl("tt40ti8_q20/stitched/jmad/sps_ti8_q20_errors.seq",ModelFileLocation.REPOSITORY, "errtab")
                
                ));
        definitionSet.add(new OpticsDefinitionImpl("TT40TI8-LHC-Q26-2021v1", new CallableModelFileImpl(
                "operation/beta0.inp", ModelFileLocation.REPOSITORY),new CallableModelFileImpl(
                "tt40ti8_q26/stitched/jmad/sps_ti8q26_savedseq.seq", ModelFileLocation.REPOSITORY),
                new CallableModelFileImpl(
                        "tt40ti8_q26/stitched/jmad/sps_ti8_q26.inp", ModelFileLocation.REPOSITORY),
                new TableModelFileImpl("tt40ti8_q26/stitched/jmad/sps_ti8_q26_errors.seq",ModelFileLocation.REPOSITORY, "errtab")

                ));
   
        return definitionSet;
    }
    
    

    @Override
    public JMadModelDefinition create() {
        // TODO Auto-generated method stub
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("TT40TI8");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setRepositoryPrefix("..");
        modelDefinition.setModelPathOffsets(offsets);

        for (OpticsDefinition opticsDefinition : createOpticsDefinitions()) {
            modelDefinition.addOpticsDefinition(opticsDefinition);
        }
       
        modelDefinition.setDefaultOpticsDefinition(modelDefinition.getOpticsDefinitions().get(0));

        /*
         * SEQUENCE
         */

       
        SequenceDefinitionImpl tt40ti8 = new SequenceDefinitionImpl("sps_ti8",null);
        modelDefinition.setDefaultSequenceDefinition(tt40ti8);
        RangeDefinitionImpl tt40ti8range = new RangeDefinitionImpl(tt40ti8, "ALL", createExtrInitialConditions());

        tt40ti8range.addPostUseFile(new CallableModelFileImpl("operation/seterrtab.madx",ModelFileLocation.REPOSITORY));
        tt40ti8.setDefaultRangeDefinition(tt40ti8range);

        return modelDefinition;
        
        
      
    }
    
    /**
     * Twiss initial conditions from extraction point
     */
    private final TwissInitialConditionsImpl createExtrInitialConditions() {
        
        
        TwissInitialConditionsImpl twissInitialConditions = new TwissInitialConditionsImpl("extrsps-twiss");
        
        twissInitialConditions.setSaveBetaName("EXTR.INITBETA0");
       
        twissInitialConditions.setCalcAtCenter(true);
        twissInitialConditions.setClosedOrbit(false);
      
        return twissInitialConditions;

    }

}
