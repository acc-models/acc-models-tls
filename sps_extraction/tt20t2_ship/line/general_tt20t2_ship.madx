!==============================================================================================
! MADX file for TT20-T2 SFTPRO optics
!
! F.M. Velotti
! based on version from AFS repository and from LSA database
!
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "TT20/T2 SHiP optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../tt20t2 tt20_repo";

/***************************************
* Make model
***************************************/

call, file = "tt20_repo/tt20t2.seq";
call, file = "./t2_protons_ship_MD.str";


/*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
 call, file = "./tt20_initial_conditions.inp";


/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;

! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();


/*******************************************************************************
 * beam
 *******************************************************************************/

Beam, particle=proton, pc=400;


/*******************************************************************************
 * twiss
 *******************************************************************************/

use, sequence = tt20t2;

select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "./twiss_tt20t2_ship_nom.tfs";

/***********************************
* Cleaning up
***********************************/
system, "rm tt20_repo";

stop;
