knob_h : macro = {
    
    use, sequence = ti2;
    

    select, flag = error, clear;
    select, flag=error, class=MBI;
    efcomp, order:=0, radius:=0.025, dknr:={0,-1.35e-4,-4.7e-4};
    
    eoption, add = true;
    select, flag = error, clear;
    select, flag=error, range = "MDLH.610104";
    efcomp, order:=0, radius:=0.025, dkn:={kick_1};

    eoption, add = true;
    select, flag = error, clear;
    select, flag=error, pattern = MDLH.610206;
    efcomp, order:=0, radius:=0.025, dkn:={kick_2};
    
   
    acibh.20804 = kick_3;
    acibh.21004 = kick_4;
    acibh.21604 = kick_5;

    !eoption, add = true;
    !select, flag = error, clear;
    !select, flag=error, pattern = MDLH.610337;
    !efcomp, order:=0, radius:=0.025, dkn:={-kick_3};

    select, flag=twiss, clear;
    select, flag = twiss, column = name, s, l, x, px, betx, alfx, dx, dpx, mux, aper_1, y, py, bety, alfy, dy, dpy, muy, aper_2;
    twiss, file="matching_output/twiss_knobs_h.tfs", beta0=initbeta0;

};

set, format="12.12f";
gamma_r = beam%ti2->gamma;
emit_x = 3.5e-6 / gamma_r;

value, emit_x;

sigma_x = sqrt(table(twiss, hor_marker, betx) * emit_x);
sigma_y = sqrt(table(twiss, ver_marker, bety) * emit_x);


/***********************************
 Macro to match TLs oscillations
 at 1 sigma - horizontal
***********************************/

create, table = knob_values, column = phase, kick_1, kick_2, kick_3, kick_4, kick_5;

match_knob(psi_k) : macro = {
    
    kick_1 = 0.0;
    kick_2 = 0.0;
    kick_3 = 0.0;
    kick_4 = 0.0;
    kick_5 = 0.0;

    xp_bar = sigma_x * sin(psi_k * pi / 180.);

    x_target = sigma_x * cos(psi_k * pi / 180.);

    xp_target = (xp_bar - (table(twiss, hor_marker, alfx) * x_target)) / table(twiss, hor_marker, betx);

    value, emit_x, sigma_x, x_target, xp_target;

    match, use_macro;
        
        vary, name = kick_1, lower = -2e-5, upper = 3e-5;
        vary, name = kick_2, lower = -2e-5, upper = 2e-5;
        !vary, name = kick_3, lower = -4.5e-5/20, upper = 4.5e-5/20;
        vary, name = kick_3, lower = -70e-6/20, upper = 70e-6/20;
        vary, name = kick_4, lower = -70e-6/19, upper = 70e-6/19;
        vary, name = kick_5, lower = -70e-6/19, upper = 70e-6/19;

        use_macro, name = knob_h;

        constraint, expr = table(twiss, hor_marker, x) * 1e4 = x_target * 1e4;
        constraint, expr = table(twiss, hor_marker, px) * 1e4 = xp_target * 1e4;

        jacobian,calls=15, bisec=3;

    endmatch;

    
    select, flag=twiss, clear;
    select, flag = twiss, column = name, s, l, x, px, betx, alfx, dx, dpx, mux, aper_1, y, py, bety, alfy, dy, dpy, muy, aper_2;
    twiss, file="matching_output/twiss_knobs_h_psi_k_liu.tfs", beta0=initbeta0;
    
    phase = psi_k;

    fill, table = knob_values;

};


/***********************************
 Macro to match TLs oscillations
 at 1 sigma - vertical
***********************************/

create, table = knob_values_ver, column = phase_v, kick_1_v, kick_2_v, kick_3_v, kick_4_v;

match_vert_knob(psi_v) : macro = {
    
    acibv.20304 = 0;
    acibv.20504 = 0;
    acibv.21104 = 0;
    acibv.21304 = 0;

    yp_bar = sigma_y * sin(psi_v * pi / 180.);

    y_target = sigma_y * cos(psi_v * pi / 180.);

    yp_target = (yp_bar - (table(twiss, ver_marker, alfy) * y_target)) / table(twiss, ver_marker, bety);

    value, emit_x, sigma_y, y_target, yp_target;

    match, sequence = ti2, beta0 = initbeta0;
        
        vary, name = acibv.20304  , lower = -0.6e-5, upper = 0.6e-5;
        vary, name = acibv.20504  , lower = -0.6e-5, upper = 0.6e-5;
        vary, name = acibv.21104  , lower = -0.6e-5, upper = 0.6e-5;
        vary, name = acibv.21304  , lower = -0.6e-5, upper = 0.6e-5;

        constraint, range = ver_marker, y = y_target, py = yp_target;

        jacobian,calls=150,tolerance=1.e-15;

    endmatch;


    kick_1_v =  acibv.20304;
    kick_2_v =  acibv.20504;
    kick_3_v =  acibv.21104;
    kick_4_v =  acibv.21304;
  
    select, flag=twiss, clear;
    select, flag = twiss, column = name, s, l, x, px, betx, alfx, dx, dpx, mux, aper_1, y, py, bety, alfy, dy, dpy, muy, aper_2;
    twiss, file="matching_output/twiss_knobs_v_psi_v_liu.tfs", beta0=initbeta0;
    
    phase_v = psi_v;

    fill, table = knob_values_ver;

};

set, format="12.10f";


i = 0;
while (i <= 6){

    k = i * 30;
    exec, match_knob($k);
    exec, match_vert_knob($k);
    i = i + 1;

};

write, table = knob_values, file = "./knob_tables/hor_knobs_liu.tfs";
write, table = knob_values_ver, file = "./knob_tables/ver_knobs_liu.tfs";

