!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!  TT60-TI2 MADX model for Q26 optics post-LS2
!!
!!  F.M. Velotti from files based on: 
!!  - M. Fraser for changes in model following TCDIL installation
!!  - C. Hessler for rematchin in 2018 for ATS LHC optics 
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

option, RBARC=FALSE;
option, echo;

title,   "TI2 model post-LS2 for SPS Q26 optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../tt60ti2 ti2_repo";

set, format="22.10e";
option, -echo, warn, info;

/***************************************
* Load model
***************************************/

call,    file = "ti2_repo/ti2.seq";
call,    file = "./ti2_q26.str";
call,    file = "ti2_repo/ti2_apertures.dbx";

option,  echo, warn, info;

beam,    sequence=ti2, particle=proton, pc= 450;
use,     sequence=ti2;


call, file="./../stitched/sps_tt60_ti2_lhc_q26.inp";



set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=Y0,
      PY=PY0;

};

exec, set_ini_conditions();

SEQEDIT, SEQUENCE=ti2;
  install, element = ver_marker, class = marker, at = 1120.5, from = ti2$start;
  install, element = hor_marker, class = marker, at = 1093.0, from = ti2$start;
	FLATTEN; 
ENDEDIT;

use,     sequence=ti2;

! Errors on b2 and b3 as measured
call, file="ti2_repo/mbi_b3_error.madx";

select, flag=twiss, clear;
select, flag=twiss, column=NAME, KEYWORD, S, L, TILT, KICK, HKICK, VKICK, ANGLE, K0L, K0SL, K1L, K1SL, K2L, K2SL, K3L, K3SL, BETX, BETY, X, PX, Y, PY, DX, DPX, DY, DPY, ALFX, ALFY, mux, muy;
set, format="22.10e";
twiss, file="./twiss_ti2_q26_nom.tfs", beta0=initbeta0; 

select, flag=survey, column=name,s,l,angle,x,y,z,theta,phi,psi,globaltilt,slot_id;
survey, file="./survey_ti2.tfs",
          x0=x0.ti2, y0=y0.ti2, z0=z0.ti2,
          theta0=theta0.ti2, phi0=phi0.ti2, psi0=psi0.ti2;

!call, file = "./match_oscillation_knobs.madx";

/***********************************
* Cleaning up
***********************************/
system, "rm ti2_repo";
