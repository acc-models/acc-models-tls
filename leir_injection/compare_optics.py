from cpymad.madx import Madx
import matplotlib.pyplot as plt

GENERAL_LSA = "./ith_ite_etl_ei/line/general_leir_injection.madx"
GENERAL_REMATCHED = (
    "./ith_ite_etl_ei_rematched/line/general_leir_injection_rematched.madx"
)


def make_twiss_optics(optics_general_file):
    madx = Madx()

    madx.call(optics_general_file, chdir=True)

    madx.use(sequence="leir_injection")

    twiss_lsa = madx.twiss(beta0="initbeta0").dframe()

    madx.quit()

    return twiss_lsa


twiss_lsa = make_twiss_optics(GENERAL_LSA)
twiss_rematched = make_twiss_optics(GENERAL_REMATCHED)

fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(twiss_lsa.s, twiss_lsa.betx, label="LSA")
axes[0].plot(twiss_rematched.s, twiss_rematched.betx, label="Rematched")
axes[1].plot(twiss_lsa.s, twiss_lsa.bety, label="LSA")
axes[1].plot(twiss_rematched.s, twiss_rematched.bety, label="Rematched")
axes[0].legend()
axes[0].set(ylabel=r"$\beta_x$ / m", xlabel="s / m")
axes[1].set(ylabel=r"$\beta_y$ / m", xlabel="s / m")


fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(twiss_lsa.s, twiss_lsa.dx, label="LSA")
axes[0].plot(twiss_rematched.s, twiss_rematched.dx, label="Rematched")
axes[1].plot(twiss_lsa.s, twiss_lsa.dy, label="LSA")
axes[1].plot(twiss_rematched.s, twiss_rematched.dy, label="Rematched")
axes[0].legend()
axes[0].set(ylabel=r"D$_x$ / m", xlabel="s / m")
axes[1].set(ylabel=r"D$_y$ / m", xlabel="s / m")
plt.show()
