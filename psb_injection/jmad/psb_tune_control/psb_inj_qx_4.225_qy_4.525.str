/**********************************************************************************
*                             MAIN QUADRUPOLES
***********************************************************************************/

kbrqf              =       0.7464068036 ;
kbrqd              =      -0.7680960306 ;

/**********************************************************************************
*                             INJECTION BUMP
***********************************************************************************/

bsw_k0l            =              0.066 ;
bsw_k2l            =                  0 ;

!-------------------------------------------------
! Ring 1
!-------------------------------------------------

kbi1ksw1l4         =      -0.0016651435 ;
kbi1ksw2l1         =      -0.0047429047 ;
kbi1ksw16l1        =      -0.0054150797 ;
kbi1ksw16l4        =      -0.0012170983 ;

k0bi1bsw1l11       =       0.2108626198 ;
k0bi1bsw1l12       =      -0.2030769231 ;
k0bi1bsw1l13       =      -0.2030769231 ;
k0bi1bsw1l14       =       0.2030769231 ;


!-------------------------------------------------
! Ring 2
!-------------------------------------------------

kbi2ksw1l4         =      -0.0016651435 ;
kbi2ksw2l1         =      -0.0047429047 ;
kbi2ksw16l1        =      -0.0054150797 ;
kbi2ksw16l4        =      -0.0012170983 ;

k0bi2bsw1l11       =       0.2108626198 ;
k0bi2bsw1l12       =      -0.2030769231 ;
k0bi2bsw1l13       =      -0.2030769231 ;
k0bi2bsw1l14       =       0.2030769231 ;


!-------------------------------------------------
! Ring 3
!-------------------------------------------------

kbi3ksw1l4         =      -0.0016651435 ;
kbi3ksw2l1         =      -0.0047429047 ;
kbi3ksw16l1        =      -0.0054150797 ;
kbi3ksw16l4        =      -0.0012170983 ;

k0bi3bsw1l11       =       0.2108626198 ;
k0bi3bsw1l12       =      -0.2030769231 ;
k0bi3bsw1l13       =      -0.2030769231 ;
k0bi3bsw1l14       =       0.2030769231 ;


!-------------------------------------------------
! Ring 4
!-------------------------------------------------

kbi4ksw1l4         =      -0.0016651435 ;
kbi4ksw2l1         =      -0.0047429047 ;
kbi4ksw16l1        =      -0.0054150797 ;
kbi4ksw16l4        =      -0.0012170983 ;

k0bi4bsw1l11       =       0.2108626198 ;
k0bi4bsw1l12       =      -0.2030769231 ;
k0bi4bsw1l13       =      -0.2030769231 ;
k0bi4bsw1l14       =       0.2030769231 ;

/**********************************************************************************
*                             BETA-BEATING CORRECTION
***********************************************************************************/

kbrqd3corr         =      -0.0082077137 ;
kbrqd14corr        =      -0.0074459888 ;

/**********************************************************************************
*                                   KNOBS
***********************************************************************************/

!-------------------------------------------------
! Ring 1
!-------------------------------------------------

! KSW bump

bi1ksw_x_mm       =         -35.00000000 ;

dkbi1ksw1l4_x     =        4.75755e-05 ;
dkbi1ksw2l1_x     =       0.0001355116 ;
dkbi1ksw16l1_x    =       0.0001547166 ;
dkbi1ksw16l4_x    =        3.47742e-05 ;

kbi1ksw1l4  := dkbi1ksw1l4_x * bi1ksw_x_mm;
kbi1ksw2l1  := dkbi1ksw2l1_x * bi1ksw_x_mm;
kbi1ksw16l1 := dkbi1ksw16l1_x * bi1ksw_x_mm;
kbi1ksw16l4 := dkbi1ksw16l4_x * bi1ksw_x_mm;

! Shaver bumps

shaverr1_x_mm      =         0.0000000000 ;
shaverr1_y_mm      =         0.0000000000 ;

dkbr1dshahl4       =       0.0002320828 ;
dkbr1dshavl4       =        9.36641e-05 ;

kbr1dshahl4 := dkbr1dshahl4 * shaverr1_x_mm ;
kbr1dshavl4 := dkbr1dshavl4 * shaverr1_y_mm ;

!-------------------------------------------------
! Ring 2
!-------------------------------------------------

! KSW bump

bi2ksw_x_mm       =         -35.00000000 ;

dkbi2ksw1l4_x     =        4.75755e-05 ;
dkbi2ksw2l1_x     =       0.0001355116 ;
dkbi2ksw16l1_x    =       0.0001547166 ;
dkbi2ksw16l4_x    =        3.47742e-05 ;

kbi2ksw1l4  := dkbi2ksw1l4_x * bi2ksw_x_mm;
kbi2ksw2l1  := dkbi2ksw2l1_x * bi2ksw_x_mm;
kbi2ksw16l1 := dkbi2ksw16l1_x * bi2ksw_x_mm;
kbi2ksw16l4 := dkbi2ksw16l4_x * bi2ksw_x_mm;

! Shaver bumps

shaverr2_x_mm      =         0.0000000000 ;
shaverr2_y_mm      =         0.0000000000 ;

dkbr2dshahl4       =       0.0002320828 ;
dkbr2dshavl4       =        9.36641e-05 ;

kbr2dshahl4 := dkbr2dshahl4 * shaverr2_x_mm ;
kbr2dshavl4 := dkbr2dshavl4 * shaverr2_y_mm ;

!-------------------------------------------------
! Ring 3
!-------------------------------------------------

! KSW bump

bi3ksw_x_mm       =         -35.00000000 ;

dkbi3ksw1l4_x     =        4.75755e-05 ;
dkbi3ksw2l1_x     =       0.0001355116 ;
dkbi3ksw16l1_x    =       0.0001547166 ;
dkbi3ksw16l4_x    =        3.47742e-05 ;

kbi3ksw1l4  := dkbi3ksw1l4_x * bi3ksw_x_mm;
kbi3ksw2l1  := dkbi3ksw2l1_x * bi3ksw_x_mm;
kbi3ksw16l1 := dkbi3ksw16l1_x * bi3ksw_x_mm;
kbi3ksw16l4 := dkbi3ksw16l4_x * bi3ksw_x_mm;

! Shaver bumps

shaverr3_x_mm      =         0.0000000000 ;
shaverr3_y_mm      =         0.0000000000 ;

dkbr3dshahl4       =       0.0002320828 ;
dkbr3dshavl4       =        9.36641e-05 ;

kbr3dshahl4 := dkbr3dshahl4 * shaverr3_x_mm ;
kbr3dshavl4 := dkbr3dshavl4 * shaverr3_y_mm ;

!-------------------------------------------------
! Ring 4
!-------------------------------------------------

! KSW bump

bi4ksw_x_mm       =         -35.00000000 ;

dkbi4ksw1l4_x     =        4.75755e-05 ;
dkbi4ksw2l1_x     =       0.0001355116 ;
dkbi4ksw16l1_x    =       0.0001547166 ;
dkbi4ksw16l4_x    =        3.47742e-05 ;

kbi4ksw1l4  := dkbi4ksw1l4_x * bi4ksw_x_mm;
kbi4ksw2l1  := dkbi4ksw2l1_x * bi4ksw_x_mm;
kbi4ksw16l1 := dkbi4ksw16l1_x * bi4ksw_x_mm;
kbi4ksw16l4 := dkbi4ksw16l4_x * bi4ksw_x_mm;

! Shaver bumps

shaverr4_x_mm      =         0.0000000000 ;
shaverr4_y_mm      =         0.0000000000 ;

dkbr4dshahl4       =       0.0002320828 ;
dkbr4dshavl4       =        9.36641e-05 ;

kbr4dshahl4 := dkbr4dshahl4 * shaverr4_x_mm ;
kbr4dshavl4 := dkbr4dshavl4 * shaverr4_y_mm ;
