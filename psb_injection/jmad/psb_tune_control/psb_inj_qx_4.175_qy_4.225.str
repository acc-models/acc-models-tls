/**********************************************************************************
*                             MAIN QUADRUPOLES
***********************************************************************************/

kbrqf              =       0.7286060909 ;
kbrqd              =      -0.7411739356 ;

/**********************************************************************************
*                             INJECTION BUMP
***********************************************************************************/

bsw_k0l            =              0.066 ;
bsw_k2l            =                  0 ;

!-------------------------------------------------
! Ring 1
!-------------------------------------------------

kbi1ksw1l4         =      -0.0015823921 ;
kbi1ksw2l1         =      -0.0045072001 ;
kbi1ksw16l1        =      -0.0051459706 ;
kbi1ksw16l4        =      -0.0011566131 ;

k0bi1bsw1l11       =       0.2108626198 ;
k0bi1bsw1l12       =      -0.2030769231 ;
k0bi1bsw1l13       =      -0.2030769231 ;
k0bi1bsw1l14       =       0.2030769231 ;


!-------------------------------------------------
! Ring 2
!-------------------------------------------------

kbi2ksw1l4         =      -0.0015823921 ;
kbi2ksw2l1         =      -0.0045072001 ;
kbi2ksw16l1        =      -0.0051459706 ;
kbi2ksw16l4        =      -0.0011566131 ;

k0bi2bsw1l11       =       0.2108626198 ;
k0bi2bsw1l12       =      -0.2030769231 ;
k0bi2bsw1l13       =      -0.2030769231 ;
k0bi2bsw1l14       =       0.2030769231 ;


!-------------------------------------------------
! Ring 3
!-------------------------------------------------

kbi3ksw1l4         =      -0.0015823921 ;
kbi3ksw2l1         =      -0.0045072001 ;
kbi3ksw16l1        =      -0.0051459706 ;
kbi3ksw16l4        =      -0.0011566131 ;

k0bi3bsw1l11       =       0.2108626198 ;
k0bi3bsw1l12       =      -0.2030769231 ;
k0bi3bsw1l13       =      -0.2030769231 ;
k0bi3bsw1l14       =       0.2030769231 ;


!-------------------------------------------------
! Ring 4
!-------------------------------------------------

kbi4ksw1l4         =      -0.0015823921 ;
kbi4ksw2l1         =      -0.0045072001 ;
kbi4ksw16l1        =      -0.0051459706 ;
kbi4ksw16l4        =      -0.0011566131 ;

k0bi4bsw1l11       =       0.2108626198 ;
k0bi4bsw1l12       =      -0.2030769231 ;
k0bi4bsw1l13       =      -0.2030769231 ;
k0bi4bsw1l14       =       0.2030769231 ;

/**********************************************************************************
*                             BETA-BEATING CORRECTION
***********************************************************************************/

kbrqd3corr         =      -0.0131398292 ;
kbrqd14corr        =      -0.0127559903 ;

/**********************************************************************************
*                                   KNOBS
***********************************************************************************/

!-------------------------------------------------
! Ring 1
!-------------------------------------------------

! KSW bump

bi1ksw_x_mm       =         -35.00000000 ;

dkbi1ksw1l4_x     =        4.52112e-05 ;
dkbi1ksw2l1_x     =       0.0001287771 ;
dkbi1ksw16l1_x    =       0.0001470277 ;
dkbi1ksw16l4_x    =        3.30461e-05 ;

kbi1ksw1l4  := dkbi1ksw1l4_x * bi1ksw_x_mm;
kbi1ksw2l1  := dkbi1ksw2l1_x * bi1ksw_x_mm;
kbi1ksw16l1 := dkbi1ksw16l1_x * bi1ksw_x_mm;
kbi1ksw16l4 := dkbi1ksw16l4_x * bi1ksw_x_mm;

! Shaver bumps

shaverr1_x_mm      =         0.0000000000 ;
shaverr1_y_mm      =         0.0000000000 ;

dkbr1dshahl4       =       0.0003156521 ;
dkbr1dshavl4       =       0.0001029168 ;

kbr1dshahl4 := dkbr1dshahl4 * shaverr1_x_mm ;
kbr1dshavl4 := dkbr1dshavl4 * shaverr1_y_mm ;

!-------------------------------------------------
! Ring 2
!-------------------------------------------------

! KSW bump

bi2ksw_x_mm       =         -35.00000000 ;

dkbi2ksw1l4_x     =        4.52112e-05 ;
dkbi2ksw2l1_x     =       0.0001287771 ;
dkbi2ksw16l1_x    =       0.0001470277 ;
dkbi2ksw16l4_x    =        3.30461e-05 ;

kbi2ksw1l4  := dkbi2ksw1l4_x * bi2ksw_x_mm;
kbi2ksw2l1  := dkbi2ksw2l1_x * bi2ksw_x_mm;
kbi2ksw16l1 := dkbi2ksw16l1_x * bi2ksw_x_mm;
kbi2ksw16l4 := dkbi2ksw16l4_x * bi2ksw_x_mm;

! Shaver bumps

shaverr2_x_mm      =         0.0000000000 ;
shaverr2_y_mm      =         0.0000000000 ;

dkbr2dshahl4       =       0.0003156521 ;
dkbr2dshavl4       =       0.0001029168 ;

kbr2dshahl4 := dkbr2dshahl4 * shaverr2_x_mm ;
kbr2dshavl4 := dkbr2dshavl4 * shaverr2_y_mm ;

!-------------------------------------------------
! Ring 3
!-------------------------------------------------

! KSW bump

bi3ksw_x_mm       =         -35.00000000 ;

dkbi3ksw1l4_x     =        4.52112e-05 ;
dkbi3ksw2l1_x     =       0.0001287771 ;
dkbi3ksw16l1_x    =       0.0001470277 ;
dkbi3ksw16l4_x    =        3.30461e-05 ;

kbi3ksw1l4  := dkbi3ksw1l4_x * bi3ksw_x_mm;
kbi3ksw2l1  := dkbi3ksw2l1_x * bi3ksw_x_mm;
kbi3ksw16l1 := dkbi3ksw16l1_x * bi3ksw_x_mm;
kbi3ksw16l4 := dkbi3ksw16l4_x * bi3ksw_x_mm;

! Shaver bumps

shaverr3_x_mm      =         0.0000000000 ;
shaverr3_y_mm      =         0.0000000000 ;

dkbr3dshahl4       =       0.0003156521 ;
dkbr3dshavl4       =       0.0001029168 ;

kbr3dshahl4 := dkbr3dshahl4 * shaverr3_x_mm ;
kbr3dshavl4 := dkbr3dshavl4 * shaverr3_y_mm ;

!-------------------------------------------------
! Ring 4
!-------------------------------------------------

! KSW bump

bi4ksw_x_mm       =         -35.00000000 ;

dkbi4ksw1l4_x     =        4.52112e-05 ;
dkbi4ksw2l1_x     =       0.0001287771 ;
dkbi4ksw16l1_x    =       0.0001470277 ;
dkbi4ksw16l4_x    =        3.30461e-05 ;

kbi4ksw1l4  := dkbi4ksw1l4_x * bi4ksw_x_mm;
kbi4ksw2l1  := dkbi4ksw2l1_x * bi4ksw_x_mm;
kbi4ksw16l1 := dkbi4ksw16l1_x * bi4ksw_x_mm;
kbi4ksw16l4 := dkbi4ksw16l4_x * bi4ksw_x_mm;

! Shaver bumps

shaverr4_x_mm      =         0.0000000000 ;
shaverr4_y_mm      =         0.0000000000 ;

dkbr4dshahl4       =       0.0003156521 ;
dkbr4dshavl4       =       0.0001029168 ;

kbr4dshahl4 := dkbr4dshahl4 * shaverr4_x_mm ;
kbr4dshavl4 := dkbr4dshavl4 * shaverr4_y_mm ;
