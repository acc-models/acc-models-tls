/******************************************************************
 * Ring 1
 ******************************************************************/


use, sequence = psb1;
seqedit, sequence=psb1;
 flatten;
  install, element= bi1.foil, class=marker, at = 0, from = bi1.tstr1l1;  
 flatten;
  cycle, start = bi1.foil;
 flatten;
endedit;

use, sequence = psb1;

seqedit, sequence = psb1;
    remove, element = bi1.tstr1l1;
endedit;
use, sequence = psb1;

/* exec, assign_KSW1_strength; */
/* exec, assign_BSW1_strength; */
/* exec, assign_BSW1_alignment; */


exec, ptc_twiss_macro(2,0,0);
xpsb01=-table(ptc_twiss,BI1.FOIL,X);

if (buncher == kev_100){
    call, file = "bi_repo/ini_cond_100kev.inp";
} elseif (buncher == kev_250){
    call, file = "bi_repo/ini_cond_250kev.inp";
} elseif (buncher == kev_450){
    call, file = "bi_repo/ini_cond_450kev.inp";
};

call, file = "bi_repo/bi_optics_r1.madx";

bi1psb1.seqlen = 157.08+48.54609800;

BI1PSB1: SEQUENCE, refer = entry, L = bi1psb1.seqlen   ;
    bi1_foil, at=0;
    psb1, at =48.54609800  ;
endsequence;

use, sequence = bi1psb1;

seqedit, sequence = bi1psb1;
    flatten;
endedit;
use, sequence = bi1psb1;

assign_errors_psb1(): macro = {
    select, flag=error, clear;
    SELECT, FLAG=ERROR, RANGE="BI1.QFO50";
    EALIGN, DY =yr1;
    select, flag=error, clear;
    SELECT, FLAG=ERROR, RANGE="BI1.QDE60";
    EALIGN, DY =yr1;

    /* exec, assign_KSW1_strength; */
    exec, bi1_macrobi1();
    /* exec, assign_BSW1_strength; */
    /* exec, assign_BSW1_alignment; */
};

