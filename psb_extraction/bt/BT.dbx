option, -echo;
option, warn;
option, info;
!----------------------------------------------------------------------------
! Directory: /afs/cern.ch/eng/ps/cps/TransLines/PSB-PS/2013
! Aperture file created in August 2012 by V.Raginel, O.Berrig and B. Mikulec
!----------------------------------------------------------------------------
! Updated for LIU 20/10/2017 by C. Hessler
! For the SMVs, KFAs, QNO10, QNO20 and QNO30 the beam passes off-center
! through the element, which is not taken into account in the given 
! apertures. Only the mechanical apertures are given!
!----------------------------------------------------------------------------


/****************************************************************************
 * BT1
 ****************************************************************************/
BT1.BVT10,     APERTYPE=ELLIPSE,    APERTURE={0.0563/2, 0.0563/2 };
BT1.BPM00,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J. Tan 24/10/2017
BT1.DHZ10,     APERTYPE=ELLIPSE,    APERTURE={0.085/2,  0.085/2  };
BT1.VVS10,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J.B. Gomes 24/10/2017

BT1.SMV10,     APERTYPE=RECTANGLE,  APERTURE={0.0604/2,  0.102/2  };! mechanical aperture, less aperture at the septum blade, see EDMS #1537199 
BT1.QNO10,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! mechanical aperture, but beam passes quad off-center
BT1.QNO20,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! mechanical aperture, but beam passes quad off-center
BT1.KFA10,     APERTYPE=RECTANGLE,  APERTURE={0.110/2,  0.053/2  };! mechanical aperture, sagitta negligible

/****************************************************************************
 * BT2
 ****************************************************************************/
BT2.DVT10,     APERTYPE=ELLIPSE,    APERTURE={0.0563/2, 0.0563/2 };
BT2.BPM00,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J. Tan 24/10/2017
BT2.DHZ10,     APERTYPE=ELLIPSE,    APERTURE={0.085/2,  0.085/2  };
BT2.VVS10,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J.B. Gomes 24/10/2017
BT2.DVT20,     APERTYPE=ELLIPSE,    APERTURE={0.085/2,  0.085/2  };

BT2.SMV10,     APERTYPE=RECTANGLE,  APERTURE={0.0604/2,  0.102/2  };! mechanical aperture, less aperture at the septum blade, see EDMS #1537199 
!BT2.BTV10 TBC !!!
BT2.BPM10,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J. Tan 24/10/2017
BT2.QNO10,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! mechanical aperture, but beam passes quad off-center
BT2.VVS30,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J.B. Gomes 24/10/2017
BT2.QNO20,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! mechanical aperture, but beam passes quad off-center
BT2.KFA10,     APERTYPE=RECTANGLE,  APERTURE={0.110/2,  0.053/2  };! mechanical aperture, sagitta negligible

BT2.BVT20,     APERTYPE=ELLIPSE,    APERTURE={0.113/2,  0.113/2  };
BT2.BPM20,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J. Tan 24/10/2017
BT2.DVT40,     APERTYPE=ELLIPSE,    APERTURE={0.095/2,  0.095/2  };

BT2.SMV20,     APERTYPE=RECTANGLE,  APERTURE={0.0604/2,  0.102/2  };! mechanical aperture, less aperture at the septum blade, see EDMS #1537199 
BT2.BTV30,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J. Tan 24/10/2017
BT2.QNO30,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! mechanical aperture, but beam passes quad off-center
BT2.KFA20,     APERTYPE=RECTANGLE,  APERTURE={0.110/2,  0.053/2  };! mechanical aperture, sagitta negligible

/****************************************************************************
 * BT3
 ****************************************************************************/
BT3.DVT10,     APERTYPE=ELLIPSE,    APERTURE={0.0563/2, 0.0563/2 };
BT3.BPM00,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J. Tan 24/10/2017
BT3.DHZ10,     APERTYPE=ELLIPSE,    APERTURE={0.085/2,  0.085/2  };
BT3.VVS10,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J.B. Gomes 24/10/2017
BT3.DVT20,     APERTYPE=ELLIPSE,    APERTURE={0.085/2,  0.085/2  };

BT3.SMV10,     APERTYPE=RECTANGLE,  APERTURE={0.0604/2,  0.102/2  };! mechanical aperture, less aperture at the septum blade, see EDMS #1537199 
!BT3.BTV10 TBC !!!
BT3.BPM10,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J. Tan 24/10/2017
BT3.QNO10,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! mechanical aperture, but beam passes quad off-center
BT3.VVS20,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J.B. Gomes 24/10/2017
BT3.VVS30,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J.B. Gomes 24/10/2017
BT3.QNO20,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! mechanical aperture, but beam passes quad off-center
BT3.KFA10,     APERTYPE=RECTANGLE,  APERTURE={0.110/2,  0.053/2  };! mechanical aperture, sagitta negligible

BT3.DVT30,     APERTYPE=ELLIPSE,    APERTURE={0.095/2,  0.095/2  };
BT3.BPM20,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J. Tan 24/10/2017
BT3.DVT40,     APERTYPE=ELLIPSE,    APERTURE={0.095/2,  0.095/2  };

BT3.SMV20,     APERTYPE=RECTANGLE,  APERTURE={0.0604/2,  0.102/2  };! mechanical aperture, less aperture at the septum blade, see EDMS #1537199 
BT3.BTV30,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J. Tan 24/10/2017
BT3.QNO30,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! mechanical aperture, but beam passes quad off-center
BT3.KFA20,     APERTYPE=RECTANGLE,  APERTURE={0.110/2,  0.053/2  };! mechanical aperture, sagitta negligible

/****************************************************************************
 * BT4
 ****************************************************************************/
BT4.BVT10,     APERTYPE=ELLIPSE,    APERTURE={0.0563/2, 0.0563/2 };
BT4.BPM00,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J. Tan 24/10/2017
BT4.DHZ10,     APERTYPE=ELLIPSE,    APERTURE={0.085/2,  0.085/2  };
BT4.VVS10,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J.B. Gomes 24/10/2017

BT4.SMV10,     APERTYPE=RECTANGLE,  APERTURE={0.0604/2,  0.102/2  };! mechanical aperture, less aperture at the septum blade, see EDMS #1537199 
BT4.QNO10,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! mechanical aperture, but beam passes quad off-center
BT4.QNO20,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! mechanical aperture, but beam passes quad off-center
BT4.KFA10,     APERTYPE=RECTANGLE,  APERTURE={0.110/2,  0.053/2  };! mechanical aperture, sagitta negligible

/****************************************************************************
 * BT
 ****************************************************************************/
!BT.VPI22,      APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! vacuum equipment without effect on aperture
!BT.VPI22A,     APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! vacuum equipment without effect on aperture
BT.BTV20,      APERTYPE=ELLIPSE,    APERTURE={0.100/2,  0.100/2  };! TBC !!!

! BT_VPI23 diameter=264mm. It is offset by -40 mm, which cannot be done by MAD
!BT.VPI23,      APERTYPE=ELLIPSE,    APERTURE={0.26400,  0.26400 };! vacuum equipment without effect on aperture
BT.BPM30,      APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J. Tan 24/10/2017
BT.BCT10,      APERTYPE=ELLIPSE,    APERTURE={0.130/2,  0.130/2  };! From J. Tan 24/10/2017
BT.DVT50,      APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };
!BT.VPG22,      APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! vacuum equipment without effect on aperture
BT.BTV40,      APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J. Tan 24/10/2017
BT.DVT60,      APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };
BT.QNO40,      APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };
BT.BPM40,      APERTYPE=ELLIPSE,    APERTURE={0.145/2,  0.145/2  };! From J. Tan 24/10/2017
BT.QNO50,      APERTYPE=ELLIPSE,    APERTURE={0.196/2,  0.196/2  };

option, -echo;
option, warn;
option, -info;
return;
