!==============================================================================================
! MADX file for PSB-BT2-BTM vertical emittance measurement optics
!
! S. Ogur, W. Bartmann, M.A. Fraser, F.M. Velotti
!==============================================================================================
option, echo;

 title, "PSB/BT2/BTM vertical emittance measurement optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Cleaning .inp output files
***************************************/

system, "rm *.inp";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../bt bt_repo";
system, "ln -fns ./../../btm btm_repo";
system, "ln -fns ./../../bty bty_repo";

/*******************************************************************************
 * Beam command
 *******************************************************************************/
 
 BEAM, PARTICLE=PROTON, PC = 2.794987;
 BRHO := BEAM->PC * 3.3356;

/*******************************************************************************
 * Macros
 *******************************************************************************/
 
set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};

write_ini_conditions(xtlgeode, pxtlgeode, beamname,filename) : macro = {

betx0 = beamname->betx;
bety0 =  beamname->bety;

alfx0 = beamname->alfx;
alfy0 = beamname->alfy;

dx0 = beamname->dx;
dy0 = beamname->dy;

dpx0 = beamname->dpx - pxtlgeode/(beam->beta);
dpy0 = beamname->dpy;

x0 = beamname->x - xtlgeode;
y0 = beamname->y;

px0 = beamname->px - pxtlgeode;
py0 = beamname->py;

mux0 = beamname->mux;
muy0 = beamname->muy;

assign, echo="filename";

print, text="/*********************************************************************";
print, text="Initial conditions from MADX model of PSB extraction to BT lines";
print, text="*********************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

value,x0 ;
value,px0 ;

assign, echo=terminal;

};


/*****************************************************************************
 Calculate initial conditions for BT transfer line
*****************************************************************************/

call, file = "bt_repo/load_psb2_extraction_lhc.madx";

! In this example the KFA14's kick can be adjusted (absolute error  in rad), sign and 
! file name to save the ring twiss 
! Returns all initial conditions needed, hence a set_ini_conditions() is needed 
! If "sign" (second argument) = 1, positive kick, negative otherwise

 set, format="22.10e";
exec, calculate_extraction(1e-3, 1, twiss_psb_stitched);    

/***********************************************************
* Define TL reference frame - to be updated with SU (GEODE) measured values
************************************************************/

! Until we have better information we assume the beam is aligned to BT in the nominal case
xtl = tl_initial_cond_nominal->x;
pxtl = tl_initial_cond_nominal->px;

/*****************************************************************************
 * BT2, BTM and BTY
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 
 option, -echo;
 call, file = "bt_repo/BT-LIU_v_emit.str";
 call, file = "bt_repo/BT.ele";
 call, file = "bt_repo/BT2_LIU.seq";
 call, file = "bt_repo/BT.dbx";
 call, file = "bty_repo/BTY_GPS.str";
 call, file = "bty_repo/BTY.ele";
 call, file = "bty_repo/BTY.seq";
 call, file = "btm_repo/BTM-LIU_v_emit.str";
 call, file = "btm_repo/BTM.ele";
 call, file = "btm_repo/BTM-LIU.seq"; 
 call, file = "btm_repo/BTM.dbx"; 
 option, echo;
 

 /*******************************************************************************
 * Combine sequences
 *******************************************************************************/
lpsbext = 139.110102;

lbt1 = 33.91783854;
lbt2 = 33.9071301;
lbt3 = 33.89615656;
lbt4 = 33.906865;

lbtm = 25.124820852;

bt2btm: SEQUENCE, refer=ENTRY, l  =  lbt2 + lbtm;
	           bt2       , at =     0;
	           btm       , at =  lbt2;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = bt2btm;
FLATTEN;
ENDEDIT;

/***********************************************************
* Save initial parameters in PSB ring for JMAD
************************************************************/
exec, write_ini_conditions(0,0,psbstart,psb2_START_V_EMIT.inp);
exec, write_ini_conditions(0,0,smhstart,smh2_START_V_EMIT.inp);

/***********************************************************
* Initialise and save initial parameters to file for TL usage (reference frame modified)
************************************************************/

exec, write_ini_conditions(xtl,pxtl,tl_initial_cond,BT2_START_KICK_RESPONSE_V_EMIT.inp);
exec, set_ini_conditions();

/*******************************************************************************
 * Run twiss for BT2-BTM and stitch result for kick response
 *******************************************************************************/

use, sequence= bt2btm;  
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta,label=tl_final_cond_kick_response, place = #e;
twiss, beta0=initbeta0;

! Make one single tfs file for both ring and BT transfer line
len_twiss_tl = table(twiss, tablelength);

i = 1;
option, -info;
while(i < len_twiss_tl){

    if(i == 1){
        SETVARS, TABLE=trajectory, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=trajectory;

    i = i + 1;
};
option, info;
write, table=trajectory, file="twiss_psb_bt2btm_v_emit_kick_response_complete.tfs";

/***********************************************************
* Save parameters at BTM dump for kick response
************************************************************/

exec, write_ini_conditions(0,0,tl_final_cond_kick_response,BTM_DUMP_V_EMIT_KICK_RESPONSE.inp);

/***********************************************************
* Initialise and save initial parameters to file for TL usage (reference frame modified)
************************************************************/

exec, write_ini_conditions(xtl,pxtl,tl_initial_cond_nominal,BT2_START_V_EMIT.inp);
exec, set_ini_conditions();

/*******************************************************************************
 * Run twiss for BT2-BTM and stitch result for nominal case
 *******************************************************************************/

use, sequence= bt2btm;  
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta,label=tl_final_cond_nominal, place = #e;
savebeta,label=btm2_start_v_emit, place = btm.start;
twiss, beta0=initbeta0;

! Make one single tfs file for both ring and BT transfer line
len_twiss_tl = table(twiss, tablelength);

i = 1;
option, -info;
while(i < len_twiss_tl){

    if(i == 1){
        SETVARS, TABLE=nominal, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=nominal;

    i = i + 1;
};
option, info;
write, table=nominal, file="twiss_psb_bt2btm_v_emit_nom_complete.tfs";

/***********************************************************
* Save parameters at BTM start for nominal case
************************************************************/

exec, write_ini_conditions(0,0,btm2_start_v_emit,btm2_START_V_EMIT.inp);

/***********************************************************
* Save parameters at BTM dump for nominal case
************************************************************/

exec, write_ini_conditions(0,0,tl_final_cond_nominal,BTM_DUMP_V_EMIT.inp);

/***********************************************************
* JMAD: prepare single sequences
************************************************************/

EXTRACT, SEQUENCE=psb2, FROM=psb2.START, TO=BR2.BT_START, NEWNAME=psb2_ext;

psbbt2btm: SEQUENCE, refer=ENTRY, L  = lpsbext + lbt2 + lbtm;
    psb2_ext        , AT =  0.0000000000 ;
	bt2          	, AT =  lpsbext ;
    btm          	, AT =  lpsbext + lbt2;
ENDSEQUENCE;

! Matrix to correct for TL reference frame shift
lm1: MATRIX, L=0,  kick1=-xtl, kick2=-pxtl, rm26=-pxtl/(beam->beta), rm51=pxtl/(beam->beta);

SEQEDIT, SEQUENCE = psbbt2btm;
FLATTEN;
INSTALL, ELEMENT = lm1, at = 0, from=BR2.BT_START;
FLATTEN;
ENDEDIT;

! Special sequence for HE corrector knob upstream up BT.SMH15

EXTRACT, SEQUENCE=psb2, FROM=BR2.DVT14L1, TO=BR2.BT_START, NEWNAME=smh_ext;

lsmh_ext = lpsbext - 127.93409;

smhbt2btm: SEQUENCE, refer=ENTRY, L  = lsmh_ext + lbt2 + lbtm;
    smh_ext        , AT =  0.0000000000 ;
	bt2          	, AT =  lsmh_ext ;
    btm          	, AT =  lsmh_ext + lbt2;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = smhbt2btm;
FLATTEN;
INSTALL, ELEMENT = lm1, at = 0, from=BR2.BT_START;
FLATTEN;
ENDEDIT;

! Ensure the kick response is OFF
kBE2KFA14L1 := kBE2KFA14L1REF;

 set, format="22.10e";
use, sequence= psbbt2btm;  
option, -warn;
save, sequence=psbbt2btm, beam, file='jmad/psbbt2btm_v_emit.jmad';
option, warn;

 set, format="22.10e";
use, sequence= smhbt2btm; 
option, -warn;
save, sequence=smhbt2btm, beam, file='jmad/smhbt2btm_v_emit.jmad';
option, warn;

 set, format="22.10e";
use, sequence= bt2btm; 
option, -warn;
save, sequence=bt2btm, beam, file='jmad/bt2btm_v_emit.jmad';
option, warn;

 set, format="22.10e";
use, sequence= btm; 
option, -warn;
save, sequence=btm, beam, file='jmad/btm_v_emit.jmad';
option, warn;

/***************************************
* Copy intial conditions to jmad folder
***************************************/

system, "cp psb2_start_v_emit.inp jmad";
system, "cp smh2_start_v_emit.inp jmad";
system, "cp bt2_start_v_emit.inp jmad";
system, "cp btm2_start_v_emit.inp jmad";

/*************************************
* Cleaning up
*************************************/

system, "rm bt_repo";
system, "rm btm_repo";
system, "rm bty_repo";
system, "rm -rf psb_repo || rm psb_repo";
stop;
