!------------------------------------------------------------------
! DI elements definitions
! The DI line starts at the AD target and ends at the injection in AD
! The particles are anti-protons at 3.575 GeV/c
!
!------------------------------------------------------------------

! Quadrupole magnets are called  QFO and  QDE
! bending magnet polarities are standard:
! positive angles mean bending to the right or downwards (if tilt = pi / 2)


! QUADRUPOLES ******************************************************

 DI.QDE6010  :   QUADRUPOLE,   L= 0.9200,      K1:= DI.KQDE6010, kmin=-0.718,  kmax=0;
 DI.QFO6020  :   QUADRUPOLE,   L= 0.9200,      K1:= DI.KQFO6020, kmin=0,       kmax=0.537;
 DI.QDE6030  :   QUADRUPOLE,   L= 0.6720,      K1:= DI.KQDE6030, kmin=-0.3814, kmax=0;
 DI.QFO6040  :   QUADRUPOLE,   L= 0.6720,      K1:= DI.KQFO6040, kmin=0,       kmax=0.3814;
 DI.QDE6050  :   QUADRUPOLE,   L= 0.6720,      K1:= DI.KQDE6050, kmin=-0.5721, kmax=0;
 DI.QFO6060  :   QUADRUPOLE,   L= 0.6720,      K1:= DI.KQFO6060, kmin=0,       kmax=0.5721;
 DI.QDE6070  :   QUADRUPOLE,   L= 1.0950,      K1:= DI.KQDE6070, kmin=-0.337, kmax=0.337;
 DI.QFO6080  :   QUADRUPOLE,   L= 1.0950,      K1:= DI.KQFO6080, kmin=-0.576, kmax=0.576;

! DIPOLES **********************************************************

! dipole angles
 DI.dBHZ6024 =  0.1227600000;
 DI.dBHZ6025 =  0.1227600000;
 DI.dBHZ6034 = -0.1241500000;
 DI.dBHZ6035 = -0.1241500000;
 DI.dBHZ6044 = -0.1240900000;
 DI.dBHZ6045 = -0.1240900000;
 DI.dBHZ6064 =  0.0836700000;
 DI.dBHZ6065 =  0.0836700000; 

! dipole magnetic lengths
 DI.lBHZ6024 = 1.590; 
 DI.lBHZ6025 = 1.590; 
 DI.lBHZ6034 = 1.461; 
 DI.lBHZ6035 = 1.461; 
 DI.lBHZ6044 = 1.461; 
 DI.lBHZ6045 = 1.461; 
 DI.lBHZ6064 = 1.470; 
 DI.lBHZ6065 = 1.470; 


! dipoles definition
 DI.BHZ6024  :   RBEND, L=DI.lBHZ6024, ANGLE=DI.dBHZ6024; !, E1=DI.dBHZ6024/2, E2=DI.dBHZ6024/2;
 DI.BHZ6025  :   RBEND, L=DI.lBHZ6025, ANGLE=DI.dBHZ6025; !, E1=DI.dBHZ6025/2, E2=DI.dBHZ6025/2;
 DI.BHZ6034  :   RBEND, L=DI.lBHZ6034, ANGLE=DI.dBHZ6034; !, E1=DI.dBHZ6034/2, E2=DI.dBHZ6034/2;
 DI.BHZ6035  :   RBEND, L=DI.lBHZ6035, ANGLE=DI.dBHZ6035; !, E1=DI.dBHZ6035/2, E2=DI.dBHZ6035/2;
 DI.BHZ6044  :   RBEND, L=DI.lBHZ6044, ANGLE=DI.dBHZ6044; !, E1=DI.dBHZ6044/2, E2=DI.dBHZ6044/2;
 DI.BHZ6045  :   RBEND, L=DI.lBHZ6045, ANGLE=DI.dBHZ6045; !, E1=DI.dBHZ6045/2, E2=DI.dBHZ6045/2;
 DI.BHZ6064  :   RBEND, L=DI.lBHZ6064, ANGLE=DI.dBHZ6064; !, E1=DI.dBHZ6064/2, E2=DI.dBHZ6064/2;
 DI.BHZ6065  :   RBEND, L=DI.lBHZ6065, ANGLE=DI.dBHZ6065; !, E1=DI.dBHZ6065/2, E2=DI.dBHZ6065/2;

! CORRECTORS *******************************************************
 DI.DVT6067  :   VKICKER    , L=0.40 ;  ! ONLY VERTICAl KICKER
 DI.DVTH6081  :   KICKER     , L=0.60 ;  ! HOR & VER KICKER

! INSTRUMENTATION **************************************************
 DI.BCT6006  :   INSTRUMENT, L=0.1175;
 DI.BTV6008  :   INSTRUMENT, L=0.27 ;  ! TV SCREEN
 DI.BTV6028  :   INSTRUMENT, L=0.56 ;  ! TV SCREEN
 DI.BTV6048  :   INSTRUMENT, L=0.56 ;  ! TV SCREEN
 DI.BTV6068  :   INSTRUMENT, L=0.70 ;  ! TV SCREEN
 DI.SLH6038  :  PLACEHOLDER, L=0.202 ;  !
 DI.COH6042  :  PLACEHOLDER, L=0.40 ;  !


! Other elements of interest - added by Davide April 2019
 FTA.TAR9065  :   MARKER;
 DI.HOR6000   :   MARKER;
 DI.COL6005   :   COLLIMATOR, L=1.6; !


! ring elements
 DR.QDC.53I  :   RBEND, L=739.6e-3, angle=-18.492e-3;
 DR.SMI5306  :   RBEND, L=1.400, angle=-139.51e-3;
 DR.BTV5308  :   INSTRUMENT, L=0;



 return;

