!==============================================================================================
! MADX file for F61-T8 proton optics
!
! N. Charitonidis, M.A. Fraser, F.M. Velotti
!==============================================================================================
option, echo;

 title, "F61-T8 proton optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../f61 f61_repo";


/*******************************************************************************
 * Beam
 *******************************************************************************/
 
 BEAM, PARTICLE=PROTON, PC = 24.0;
 BRHO := BEAM->PC * 3.3356;

/*****************************************************************************
 * F61 and T8
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 
 option, -echo;
 call, file = "f61_repo/f61_proton.str";
 call, file = "f61_repo/f61.ele";
 call, file = "f61_repo/f61.seq";
 call, file = "f61_repo/f61.dbx"; 
 option, echo;

/*******************************************************************************
! Set initial twiss parameters
 *******************************************************************************/
 
call, file = "./f61_start_proton.inp";

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();


/*******************************************************************************
 * Twiss
 *******************************************************************************/

use, sequence = f61t8; 
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_f61_t8_proton_nom.tfs";


/*************************************
* Survey
*************************************
set_su_ini_conditions(xx) : macro = {

 x00 = table(survey,xx,X);
 y00 = table(survey,xx,Y);
 z00 = table(survey,xx,Z);
 theta00 = table(survey,xx,THETA);
 phi00 = table(survey,xx,PHI);
 psi00 = table(survey,xx,PSI);

 };
 
call, file = "./make_survey_f61.madx"; 
*************************************/

/***********************************
* Cleaning up
***********************************/
system, "rm f61_repo";

stop;
