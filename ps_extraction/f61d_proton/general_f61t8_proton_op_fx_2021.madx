!==============================================================================================
! MADX file for F61-T8 proton optics
!
! N. Charitonidis, M.A. Fraser, F.M. Velotti, E.P. Johnson
!==============================================================================================
option, echo, update_from_parent;

title, "F61D proton optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../f61d f61d_repo";


/*******************************************************************************
 * Beam
 *******************************************************************************/
 
 BEAM, PARTICLE=PROTON, PC = 24.0;
 BRHO := BEAM->PC * 3.3356;

/*****************************************************************************
 * F61 and T8
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 
 option, -echo;
 call, file = "f61d_repo/f61d_proton.str";
 call, file = "f61d_repo/f61d.ele";
 call, file = "f61d_repo/f61d.dbx";
 call, file = "f61d_repo/f61d.seq";
 option, echo;

/*******************************************************************************
! Set initial twiss parameters
 *******************************************************************************/
 
call, file = "./f61_start_proton.inp";

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};

exec, set_ini_conditions();

/*******************************************************************************
! Write initial twiss parameters using INITBETA0
 *******************************************************************************/
 
write_ini_conditions(xtlgeode, pxtlgeode, beamname,filename) : macro = {

betx0 = beamname->betx;
bety0 =  beamname->bety;

alfx0 = beamname->alfx;
alfy0 = beamname->alfy;

dx0 = beamname->dx;
dy0 = beamname->dy;

dpx0 = beamname->dpx - pxtlgeode/(beam->beta);
dpy0 = beamname->dpy;

x0 = beamname->x - xtlgeode;
y0 = beamname->y;

px0 = beamname->px - pxtlgeode;
py0 = beamname->py;

mux0 = beamname->mux;
muy0 = beamname->muy;

assign, echo="filename";

print, text="/*********************************************************************";
print, text="Initial conditions from Maptrack model of PS SX (no main unit fringe)";
print, text="*********************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

value,x0 ;
value,px0 ;

assign, echo=terminal;

};

/*******************************************************************************
 * Twiss
 *******************************************************************************/
use, sequence = f61d; 
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APERTYPE, APER_1,APER_2, APER_3, APER_4, K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_f61d_proton_nom.tfs";

/*******************************************************************************
 * Dump twiss to JMAD folder
 *******************************************************************************/

set, format="22.10e";
use, sequence= f61d;  
option, -warn;
save, sequence=f61d, beam, file='jmad/f61d_proton.jmad';
option, warn;

/*******************************************************************************
 * Write initial conditions to file
 *******************************************************************************/

exec, write_ini_conditions(0,0,initbeta0,f61_start_proton.inp);

/***************************************
* Copy intial conditions to JMAD folder
***************************************/

system, "cp f61_start_proton.inp jmad";

/***********************************
* Cleaning up
***********************************/
system, "rm f61d_repo";

stop;