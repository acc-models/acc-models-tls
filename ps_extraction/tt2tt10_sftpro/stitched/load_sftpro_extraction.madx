/***********************************************************************
 * MAD-X script to load MTE extraction from PS up to SPS injection
 **
 ** F.Velotti, M. Fraser, A. Haushauer
 ***********************************************************************/

option, RBARC=FALSE;

/******************************************************************
 * Call lattice files
 ******************************************************************/
option, -warn;
call, file="ps_repo/ps_mu.seq";
call, file="ps_repo/ps_ss.seq";
call, file="ps_sft_repo/ps_ext_sftpro.str";
call, file="ps_extr_repo/fringe_field_fix.madx";
call, file = "ps_extr_repo/ft16.ele";
option, warn;


/******************************************************************
 * Twiss with bump on
 ******************************************************************/

use, sequence=PS;
twiss, file = "twiss_ps_core.tfs";
PSL = table(summ, length);

PS4: SEQUENCE, L = 4*PSL, refer = entry;
PS, at = 0.0;
PS, at = PSL;
PS, at = 2*PSL;
PS, at = 3*PSL;
ENDSEQUENCE;


use, sequence=PS4;

x1 = 0.051;
px1 = -0.0016;

select, flag=ptc_twiss, column=name,keyword,s,x,px,y,py,t,pt,beta11,alfa11,beta22,alfa22,disp1,disp,disp3,disp4,gamma11,gamma22,mu1,mu2,energy,l, dpx, dpy;

ptc_create_universe;
ptc_create_layout, time=false, model=2, method=6, nst=3, exact=true;
savebeta, label=start_ps_island, place=ps$start;
ptc_twiss, closed_orbit, icase=56, no=2, table=ptc_twiss, summary_table=ptc_twiss_summary, x = x1, px = px1, file="twiss_ps_4islands.tfs";
ptc_end;

readtable, file = "./twiss_ps_4islands.tfs", table = twiss_inslands;

xi0 = table(twiss_inslands, ps$start, x);
pxi0 = table(twiss_inslands, ps$start, px);

betxi0 = table(twiss_inslands, ps$start, beta11);
alfxi0 = table(twiss_inslands, ps$start, alfa11);

betyi0 = table(twiss_inslands, ps$start, beta22);
alfyi0 = table(twiss_inslands, ps$start, alfa22);

dxi0 = table(twiss_inslands, ps$start, disp1);
dpxi0 = table(twiss_inslands, ps$start, dpx);

dyi0 = 0.0;
dpyi0 = 0.0;

start_ps_island: BETA0,
  BETX=BETXi0,
  ALFX=ALFXi0,
  MUX=0.0,
  BETY=BETYi0,
  ALFY=ALFYi0,
  MUY=0.0,
  X=Xi0,
  PX=PXi0,
  Y=0.0,
  PY=0.0,
  T=T0,
  PT=PT0,
  DX=DXi0,
  DPX=DPXi0,
  DY=DYi0,
  DPY=DPYi0;

show, start_ps_island;
use, sequence = PS;
select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta, label=bumped, place = PR.BPM23;
savebeta, label=bpm17_beta0, place = PR.BPM17;
twiss, file = "twiss_ps_island.tfs", beta0=start_ps_island;

/**********************************************
* Make extraction sequence to get to where there
* is the handover with TT2
**********************************************/

ksmh16_error = -31e-3;

SEQEDIT, sequence=PS;
FLATTEN;
ENDEDIT;

use, sequence = ps;
! Cycle sequence and changing starting point

SEQEDIT, sequence=PS;
CYCLE, START=PR.BPM17;
FLATTEN;
ENDEDIT;

use, sequence=PS;
twiss, beta0=bpm17_beta0;

start_psej = table(twiss, PR.BPM23, s);
end_psej = table(twiss, PE.BTV16, s);

len_psej = end_psej - start_psej;

value, len_psej;

/*******************************************
* Install septum and consider fringe field
*******************************************/

len_ft16 = 7.701173928;

SEPTUM16E: MARKER;
FT16: sequence, refer=entry, l=len_ft16;
MTV001        ,at=0.0;
SEPTUM16      ,at=0.0;
SEPTUM16E       ,at=2.700072902;
D16STRAY      ,at=2.700072902;
F16SHIM       ,at=4.904893572;
pointR        ,at=7.701173928;
ENDSEQUENCE;

! POINTR is the handover point between PS and TT2 => initial conditions

/***********************************
* PS_EJ sequence definition
***********************************/

EXTRACT, sequence=PS, FROM=PR.BPM23, TO=PE.BTV16, newname=PS_EJ;


PS_EXTRACT: sequence, refer=entry, l=len_psej + len_ft16 ;
PS_EJ,       at=0.0;
FT16,        at=len_psej;
ENDSEQUENCE;

use, sequence = PS_EXTRACT;

twiss, beta0 = bumped;


calculate_extraction(ring_twiss_file) : macro = {

    create,table=trajectory, column=_NAME,S,L, _KEYWORD, BETX,ALFX, x, px, dx, dpx, MUX,BETY,ALFY,Y,DY,PY,DPY,MUY, k1l;
    use, sequence = PS_EXTRACT;

    exec, septum16_on();
    ! Add nominal kick from extrction kicker
    twiss, beta0 = bumped;

    x_stray_field = table(twiss, SEPTUM16E, X);
    px_stray_field = table(twiss, SEPTUM16E, PX);

    exec, place_stray_field(x_stray_field, px_stray_field);

    twiss, beta0 = bumped, table=twiss_nom;
    
   
    ! Add nominal kick + delta kick
    savebeta,label=initial_cond, place = POINTR;
    select, flag = twiss, clear;
    SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX, X, DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
    twiss, beta0 = bumped;


    len_twiss = table(twiss_nom, tablelength);
    value, len_twiss;

    i = 2;
    option, -info;
    while(i < len_twiss){

        SETVARS, TABLE=twiss_nom, ROW=i;
        x0 = x;
        px0 = px;
        SETVARS, TABLE=twiss, ROW=i;
        x = x - x0;
        px = px - px0;

        fill, table=trajectory;

        i = i + 1;
    };
    option, info;
    write, table=trajectory, file="ring_twiss_file";
    
    betx0 = initial_cond->betx;
    bety0 =  initial_cond->bety;

    alfx0 = initial_cond->alfx;
    alfy0 = initial_cond->alfy;

    dx0 = initial_cond->dx;
    dy0 = initial_cond->dy;

    dpx0 = initial_cond->dpx;
    dpy0 = initial_cond->dpy;

    x0 = x;
    y0 = initial_cond->y;

    px0 = px;
    py0 = initial_cond->py;

    mux0 = initial_cond->mux;
    muy0 = initial_cond->muy;
    print, text="here";
    value, x0, px0;
};





